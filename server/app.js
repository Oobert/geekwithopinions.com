let Hapi = require('@hapi/hapi');

const server = Hapi.server({
  host: '0.0.0.0',
  port: 9000
});

server.ext('onPreResponse', (req, h) => {
  const { response } = req;
  if (response.isBoom && response.output.statusCode === 404) {
    return h.file('public/404/index.html');
  }
  return h.continue;
});

const start = async () => {

  await server.register(require('@hapi/inert'));

  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
      directory: {
        index: true,
        path: 'public'
      }
    }
  });



  await server.start();

  console.log('Server running at:', server.info.uri);
};

start();