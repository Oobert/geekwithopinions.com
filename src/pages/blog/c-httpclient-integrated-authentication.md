---
path: "/2014/07/14/c-httpclient-integrated-authentication/"
date: "2014-07-14T14:41:36Z"
title: "C# HttpClient integrated authentication"
categories: ["Code Things", "General"]
tags: ["C#", "Programming"]
excerpt: "HttpClient has become the standard way to make http requests in C#. This is mainly used for API cal..."
---

HttpClient has become the standard way to make http requests in C#. This is mainly used for API calls but has other uses as well. Recently, I have had to make http requests to servers that require authentication and the documentation on how to do this is scattered. The funny part is that it is really easy to do.

```csharp

var uri = new Uri("&lt;url&gt;");

var credentialCache = new CredentialCache();
credentialCache.Add(
    new Uri(uri.GetLeftPart(UriPartial.Authority)),
            "&lt;auth method&gt;",
            new NetworkCredential("&lt;user name&gt;", "&lt;password&gt;", "&lt;domain&gt;")
            );

HttpClientHandler handler = new HttpClientHandler();
handler .Credentials = credentialCache;
var httpClient = new HttpClient(handler );

var response = httpClient.GetAsync(uri).Result;
```

<auth method> can be basic, digest, ntlm, or negotiate. Then just updated the Network Credentials to that of the user you want to make the call and you are good to go.

It appears that kerberos on its own does not work. This may be because of my server configuration. However if you use negotiate, HttpClient will use kerberos if the server is configured for it otherwise it will fallback to NTLM.