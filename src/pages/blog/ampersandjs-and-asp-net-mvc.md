---
path: "/2014/11/13/ampersandjs-and-asp-net-mvc/"
date: "2014-11-13T16:26:14Z"
title: "Ampersandjs and ASP.NET MVC"
categories: ["Code Things"]
tags: ["Ampersand", "ASP.NET MVC", "C#", "JavaScript", "npm", "Programming"]
excerpt: "Ampersandis a newish client JavaScript framework that helps build native web applications. It is bu..."
---

[Ampersand](http://ampersandjs.com/ "Ampersandjs")is a newish client JavaScript framework that helps build native web applications. It is built and maintained by [&yet](https://andyet.com/ "&yet"). Ampersand is different than other frameworks because it is not a monolithic library that solves all your problems. It is a collections of small modules that all solve a single problem that when put together make up Ampersand. This allows you to easily switch out modules that don't work for you. To read more about Ampersand and its benefits check out the following links.

* [Introducing Ampersand](http://blog.andyet.com/2014/06/25/introducing-ampersand-js "Introducing Ampersand") - Intro to Ampersand. Explains why &yet created and what they were hoping to achieve.
* [Opinionated Rundown of JS Frameworks](http://blog.andyet.com/2014/08/13/opinionated-rundown-of-js-frameworks "Opinionated Rundown of JS Frameworks") - Comparison of popular JS frameworks from &yet's point of view.
* [Human JavaScript](http://read.humanjavascript.com/ "Human Javascript") - Book written about team based client side development with insights to what Ampersand gives you. READ THIS!

I recently wanted to know what it would take to run the Ampersand-cli demo app using ASP.NET MVC instead of node.js. So I did just that. The can be found on [my github](https://github.com/Oobert/Ampersand-and-ASP.NET-MVC/tree/master "Ampersand and ASP.NET MVC"). Head over there and check it out. What follows is some the key points that is a bit different than the node.js version.

**Getting Started**

Node.js is required when setting up my demo code. It is not required but I used it because it is easier if you do. Node allows us to use npm and tools like gulp and browserify which we will cover in a bit.

First we need to install gulp and browserify from npm. Navigate to the WebApp root folder from the command line and run:

<pre>npm install -g gulp browserfiy</pre>

Next install all the dependencies found in the package.json from npm by running

<pre>npm install</pre>

Next run gulp.

<pre>gulp</pre>

Finally build and run the visual studio project.

**Browserify and gulp **

Ampersand [highly suggests](http://ampersandjs.com/learn/npm-browserify-and-modules "learn npm browserify") to use a common JS loader like browserify. In my example, I followed this suggestion. [Browserify](http://browserify.org/ "browserify") is a commonJS loader that allows you to require('') modules, like node, in the browser. [Gulp](http://gulpjs.com/ "gulp")is a streaming build system that allows you to run jobs such as pre-compiling CSS, copying files, and many other things.

When browserify is pointed at our main app.js file (Scripts\App\app.js) and run, it will bundle all of the app's JavaScript files into one file (Scripts\app.bundle.js).

Gulp and the corresponding gulpfile.js is used to watch the JavaScript files and automatically run browserify when things change. This means that as you edit files, gulp will rebuild the app.bundle.js file. All you have to do is reload the browser to get the final results.

**Routes**

With any native web application frame, the odds are good you are doing routing on the client side. Which means the server must serve the same html, css, and js for any page that is request. To do this, a catch all route is created.

```csharp

routes.MapRoute(
    name: "Default",
    url: "{*.}",
    defaults: new { controller = "Home", action = "Index"}
);
```

**Base Layout and Action**

Notice the _Layout.cs file is basically empty. This is because everything is loaded via Ampersand including anything in <head>. This also means that our default controller and action serve nothing. The only things our default action needs to serve is any CSS and JavaScript.

**Templates**

Ampersand's demo site uses Jade templates and complies them at runtime down to a single template.js file. Meaning that all HTML is served to the user the first time the application loads. In my examples, I sort of replicated this by creating an Action that creates a single JS file with all the HTML templates. This can be better and more automated but for the purposes of this demo I stopped here to show a direction that could be taken.

See the Template Controller and the views in the Template view folder.

Templates do not need to be served this way. You could make HTTP get requests for them in Ampersand. I did it this way to keep in the spirit of the original demo.

**Sample API**

The simple API was created using Web API. Nothing really different going on here expect that because of C# our models are strongly typed.

**CSS**

The original Ampersand demo used stylizer and a CSS preprocessor. I took the output from that and put it into site.css. You could do whatever suits your needs here.

**Final things to Note**

In this code, the app.bundle.js and template JavaScript is not far future cached. It could be. That is something you will need to figure out. There are many different ways to do this in MVC, I did not want to suggest one. The same goes for the CSS.

Much like Nuget's packages folder, the node_modules folder is not checked into source control. Running npm install command will repopulate this folder much like Nuget's auto restore.

Other than what is about, the rest of the application is vanilla Ampersand, no other changes were made.

Source Code: [https://github.com/Oobert/Ampersand-and-ASP.NET-MVC/tree/master](https://github.com/Oobert/Ampersand-and-ASP.NET-MVC/tree/master "github")