---
path: "/2020/05/21/surveillance-software"
date: "2020-05-21T14:14:27Z"
title: "Snippets from Slack: On Surveillance Software"
categories: [""]
tags: ["Slack-ing"]
excerpt: ""
---

From DHH on Twitter
> So we wrote all the employee-surveillance software vendors, when we discovered they were using our APIs, and told them to stop. One wrote us back, essentially: "We agree! Surveillance is bad. But customers asked for it so... 🤷‍♂️. Are we OK??". No.

Source: https://twitter.com/dhh/status/1263245004815757312

A good software company/team gives the client what they want.

A great software company/team gives the client what they need.

Full stop, no one really needs these features. It is a crutch at best.

A parallel. Data Grids make for some of the worst UX today. They present data poorly and are clunky in the best of cases. Giving the client what they want is to give them a data grid. Why? Because it is what they know, trust and because they can see all their data right there. Giving a client what they need, is to build a system that solves their problem. A better UX. What is that way? No idea, they are not my client. I haven't taken the time to figure out they need.

To come back to surveillance software. It is the data grid of tracking. It works but it doesn’t really tell you what you need to know.

Notice how the difference between want and need is unclear? Yeah? Great teams can make it clear. Want != Need.