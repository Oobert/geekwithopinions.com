---
path: "/2014/06/17/why-having-an-open-door-isnt-enough/"
date: "2014-06-17T14:27:16Z"
title: "Why having an open door isn't enough"
categories: ["General"]
tags: ["Career", "Soft Skills"]
excerpt: "Recently Jason Fried wrote an article titled \"Is Your door Really Always Open?\" In it, he suggest..."
---

Recently [Jason Fried](https://twitter.com/jasonfried "twiiter") wrote an article titled ["Is Your door Really Always Open?"](http://www.inc.com/magazine/201311/jason-fried/what-open-door-policies-actually-mean.html "Is Your Door Really Always Open?") In it, he suggests that having an open door policy is great but also a cop-out. And I agree with him. So much so that I would like to go a little deeper as to why this is a problem.

I am an introvert and a bit shy. because of those traits, it is going to take a lot for me to go into my bosses office and talk. I am not going to do this unless I absolutely have to or I know it is likely that my point will be made. Recently I came across some videos called ["The Power of Introverts"](http://www.youtube.com/watch?v=74kBqeq__OQ "The power of introverts") that suggests that this is a trait of introverts as a whole. They tend to be more calculated when they speak up and wont do so unless they know they will be heard.

What this means to a manager is that having an open door isn't enough. It means that many of the employees a managers has are not just going randomly walk into your office and talk.

The other problem that I have seen with open door policies is that employees only use them to bring up problems or things they want to change.

> Why don't you get open honest feedback from your people? Because when you didn't like it, you accused them of "not being a team player".
> 
> — Ed Weissman (@edw519) [May 8, 2014](https://twitter.com/edw519/statuses/464395630903689216)

<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

If an employee is to honest to often they get labeled. Labels are bad. It is hard to remove labels. Labels keep employee opening up because they don't want the label. 

As an employee, these are the main two things that keep me from bursting into my bosses office and being honest. As an employee, the easiest way to fix these problems is to do what Jason Fried suggested in his article. That is, as a manager/boss, you need to be the one to open the communication. You need to go out and actively communicate with your employees. Once they get comfortable, they will be honest and more willing to come to you about anything. This has the added bonus of hearing more than just the negative things going on in the office.