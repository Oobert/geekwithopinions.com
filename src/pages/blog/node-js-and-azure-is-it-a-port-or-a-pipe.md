---
path: "/2013/12/09/node-js-and-azure-is-it-a-port-or-a-pipe/"
date: "2013-12-09T15:53:51Z"
title: "Node.js And Azure: Is it a Port or a Pipe?"
categories: ["New to me"]
tags: ["Azure", "Programming"]
excerpt: "When deploying anode.js application to a azure website, the node.js environment variable process.en..."
---

When deploying a[node.js](http://nodejs.org/ "nodejs") application to a azure website, the node.js environment variable process.env.port is set. One would think that port, is a the listening port and write something like this:

```js

var port = process.env.port | 8080;
```

The problem? In azure website, port is not an int, it is a string. More problematic, it is not a port at all. It is a pipe. It looks something like this:

```js

\\\\.\\pipe\\2f95e604-fc02-4365-acfc-010a26242d02'
```

The good news, node.js can handle this. Be careful though, many modules for node.js are not expecting this to be the case and me have to be set up and initialized differently than the standard IP/Port examples. [Hapi](http://spumko.github.io/ "Hapijs") and [Express](http://expressjs.com/ "Express") for example can and have  run on azure.