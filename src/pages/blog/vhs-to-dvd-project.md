---
path: "/2009/05/10/vhs-to-dvd-project/"
date: "2009-05-11T00:27:23Z"
title: "VHS to DVD project."
categories: ["New to me", "Project Updates"]
tags: ["life", "video"]
excerpt: "The Problem: What to do with all those aging home videos that are on VHS (or other analog) format?T..."
---

The Problem: What to do with all those aging home videos that are on VHS (or other analog) format?

The Answer: Buy some new toys and convert the analog video to something modern like DVDs!

My parents, like many, bought a home video camera in the late 80s. Since then my family has accumulated hours upon hours of home movies. There is a lot of family history on those VHS tapes. That is a lot of family history many do not want to disappear. The real problem is most (none technical people) do not realize that tapes do not last forever and VCRs will not be around that much longer.

I was pretty much going into this blind. There are a many possible way to get analog video converted to digital video. Since I didn't want to screw this up or waste money on anything that yielded problems or poor quality, I made sure to do my research. [VideoHelp.com](http://www.videohelp.com/ "http://www.videohelp.com/") has proven to be a great resource for all things video. There is a ton of information on the site but be warned, [VideoHelp.com](http://www.videohelp.com/ "http://www.videohelp.com/") has a large community of professionals so it is hard to get a clear answer for the hobbyist.

I started my research with exactly how to get the analog video from the VCR to my computer. There are tons of ways to convert analog video to digital video. There are a number of hardware to software and internal and external solution to do this. After reading the net, it becomes clear the easiest way to get video from analog to digital was to use a [GrassValley ADVC110](http://desktop.grassvalley.com/products/ADVC110/index.php). The ADVC110 takes in S-Video or Composite connectors. It will do the conversion from analog video to digital video in the unit and outputs the video from the device over fire-wire. The video that is output from the device is DV-AVI and runs about 13GB/hour.

The next major part of this plan was to find a VCR. There are a lot of options for a VCR too. Finding a VCR proved to be even more complicated than finding a device to do the video converter. It seems that there are a lot of opinions how what type of VCR is better and why. There is a large debate over consumer vs professional VCRs and which one is better. At the present moment in time, there are no companies making standalone VCR players. The options are to buy a used[professional or prosummer VCR](http://forum.videohelp.com/topic347374.html) from EBay or similar place or you can buy a consumer dvd/vcr combo from your local retailer. The main difference between the two options (form what I can tell) is a some of the professional VCRs will have image correct/stabilization in the form of a[TBC or Time Base correction device.](http://en.wikipedia.org/wiki/Time_Base_Corrector) A TBC will make jumpy video more stable and remove some of the artifacts form the video.

Since I needed a new DVD player for my office, I opted to go with the consumer DVD/VCR combo. I bought a Philips DVD/VHS combo from Wal-Mart for $80 bucks. I figured if the video quality was poor, I would try to find a used professional VCR. After all my research and worrying over what to buy, I finally pulled the trigger and bought the ADVC110 and VCR/DVD player. Last weekend, I hook it all up and started to convert video. I have 13 family video tapes to convert and since then I have done 3 of the tapes. I wanted the DVDs to look as good as the VHS tapes when being played on a TV. I can say that with my current set up the digital video looks better than the VHS tapes. It is all really easy to use too!

Set Up of the VCR and ADVC110 is very easy and easy to work with. The hardware set up consisted of connecting the VCR to the ADVC110 and the ADVC110 to my computer. To capture the video being pushed from the ADVC110, I use a free tool called [WinDV](http://windv.mourek.cz/) which is very easy to use. Once the raw DV-AVI video is captured, I have been using Windows Movie Maker and Windows DVD Maker to create movies and DVDs. Both are straight forward to use. They are not very robust but they get the job done to my needs.

I am open to question just remember I am by no means an expert on this subject. [Here](http://www.geekwithopinions.com/media/Sample.avi "Sample video") is a very small example of raw video that has been converted from on of the tapes. The clip is what my bedroom looked like when I was little.

Tony