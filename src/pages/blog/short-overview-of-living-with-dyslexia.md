---
path: "/2013/05/15/short-overview-of-living-with-dyslexia/"
date: "2013-05-16T00:40:21Z"
title: "Short overview of living with Dyslexia."
categories: ["General"]
tags: ["Career", "Dyslexia", "life", "Programming"]
excerpt: "I have 3 learning Disabilities: Dyslexia, Dysnomia, and Dysgraphia. What follows is what it has bee..."
---

I have 3 learning Disabilities: Dyslexia, Dysnomia, and Dysgraphia. What follows is what it has been like for me over the course of my life.

During the first years of my schooling, I mirror wrote perfectly. My teachers claimed this was a phase and I would grow out of it. To some extent this is true but it is also a sign of a learning disability. At this time, I was not reading at a the level I should have been. My teachers told my parents they needed to read with me more. So they did. For 1st and 2nd grade, things followed the same path. My parents and I were frustrated.

Then I got lucky. My 3rd grade teacher, at the risk of her job, told my parents that I should be tested for Dyslexia. Her husband has it and I showed many of the signs. My parents had me tested by independent testing center and I was diagnosed with severe Dyslexia. I was lucky because I was on the path of falling through the cracks. To this day, after meetings and being tested many times, the school district I attended does not admit to me having any learning disability. I don't know why but point is I was lucky. Many others are not as fortunate.

Since then, I was tutored outside of school. Since the district has never acknowledged my Dyslexia they did not help. Yes we tried. At a very early age, I knew what college I was going to. UW-Oshkosh. Why? Because they have one of the best programs for learning disabled students. I graduated with honors, a fair amount of student loan dept, and a Job. Fast forward to present day, I am working as a Software Engineer doing product development for a small IT shop.

During the years before college, there was a distinct difference between how teachers acted and how administration staff acted. To this day, I don't know why the school district worked so hard to keep me from being labeled as LD. Even with that, all the teachers I had were more than willing to accommodate my disability. It was refreshing. It was the little things that made a big difference. Like most of them let me type papers instead of hand write them. Remember this was the days before computers were in every house.

In college, things got even better. Attending a school with a high percentage of learning disabled students was great. There was already a process in place set by the school to help LD students and the entire staff knew about it. On top of that, for the first time I was among other students who were like me. Since I was never in any special ed classes until college, I rarely met people like me. This was awesome. I had peers. I was not alone.

I have always been comfortable with telling people of my disabilities and have never tried to hide it. This continued as I entered my professional life. I don't hide and in fact I am upfront with any potential employers. I explain to them what I have, how it affects me, and how it likely be show up in my day to day activities. Again, I have found that being up front is a lot less stressful than worrying about being "caught".

Generally speaking, I have found that that being open and honest about being Dyslexic is the best course of action. People will generally do the right thing. It also helps that I don't use my disabilities to take advantage of people kindness. Just because I have Dyslexia doesn't mean I want to be treated differently. What I want is to do tasks I am good at and can do well.

Not everything is unicorns and rainbows. I still read and write at a very low grade level. It has been a while since I have been tested but I would guess I am still below a High School level. Not being able to speak a simple word in front of a customer is always fun. Then there is the issue of when people try to sympathise.

I know people mean well when they say things like "I read slow too" or "My spelling sucks too" but they have no idea what it is like. Worse than that is when people try to marginalize what it is like. For example: I dislike doing software documentation, well any documentation. I know it needs to get done and it is part of my job. It gets done but I hate it. Really hate. When I explain this to people, some will tell me they hate it too or everyone hates it. What they don't understand is I don't hate because it is documentation, I hate it because writing for me is terribly difficult for me. I would much rather being doing stuff I am good at. Unfortunately trying to explain this to people comes off as whining or excuses. Someday I will figure it out.

All in all, these are small gripes in the grand scheme of life.

This was very brief and could easily go in great detail. I know. This is the start of a topic I hope to cover as I write more. Comments or questions always welcome.

For more infomation on the learning disabilities that I and many others live with check out the followng links:
[http://en.wikipedia.org/wiki/Dyslexia](http://en.wikipedia.org/wiki/Dyslexia)

[http://en.wikipedia.org/wiki/Dysnomia_(disorder)](http://en.wikipedia.org/wiki/Dysnomia_(disorder))

[http://en.wikipedia.org/wiki/Dysgraphia](http://en.wikipedia.org/wiki/Dysgraphia)

[http://www.interdys.org/](http://www.interdys.org/)