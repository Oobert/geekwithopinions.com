---
path: "/2020/04/11/slacking-interviewing"
date: "2020-04-11T14:14:27Z"
title: "Snippets from Slack: On interviewing"
categories: [""]
tags: ["Slack-ing"]
excerpt: ""
---

A thing I am learning: There is a good and bad way to do everything as an interviewer. And an interviewee has different talents and skills. As much as I want a consistent process as an interviewer, that isn’t fair to the interviewee. The interview should take a course that extracts as much info from the interviewee as possible. Meaning the fairest thing to do is to interview every person as an individual. This takes time and effort but would likely produce the best results.