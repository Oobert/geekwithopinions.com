---
path: "/2014/01/06/book-review-rework-and-remote/"
date: "2014-01-06T21:04:38Z"
title: "Book Review: Rework and Remote"
categories: ["Book Club"]
tags: ["Books", "Career"]
excerpt: "I am reviewing two books: Reworkand Remoteby Jason Friedand David Heinemeier Hansson of 37Signals f..."
---

I am reviewing two books: [Rework](http://37signals.com/rework/ "REWORK")and [Remote](http://37signals.com/remote/ "Remote")by [Jason Fried](https://twitter.com/jasonfried "Jason Fried")and [David Heinemeier Hansson](https://twitter.com/dhh "David Heinemeier Hansson") of [37Signals](http://37signals.com "37Signals") fame. Both books are laid out in the same form. They have multiple sections and each section has multiple chapters. Chapters are short: 1-3 pages normally. These short chapters are great because they are packed full of info without being overly long and boring. The minor down side is some chapters left me wanting more on the subject. Each chapter has an illustration that goes with it. Some of these are hilarious.  I had no problems finishing these books and staying engaged while reading them.

Both books are not made up theory that sounds nice either. Both books are rooted in what makes [37Signals](http://37signals.com "37Signals")work. The ideas and concepts in the books come straight from day to day life at [37Signals](http://37signals.com "37Signals").

**Rework**

[Rework's](http://37signals.com/rework/ "REWORK")tagline is "Change the way you work forever". The general idea is to challenge the status quo of what work should be and look like. It pushes the standard norms of running a business. For example the chapter "Why Grow?" discusses the idea of the right sized business. It suggests to the reader to find the right size for them and to stay there. This is different then the status quo of: If you are not growing you're dying.

**Remote**

[Remote's](http://37signals.com/remote/ "Remote")tagline is "Office not Required". It could be considered a playbook for setting up and having remote employees. I would suggest this for both employees or employers that want or even are working remotely. The great part about this book is it makes it clear what the trade offs are between working remotely vs being in office. In many cases it suggests why these trade offs are invalid or how to deal with them. The chapter "The Lone Outpost" suggest that giving one employee the ability to work remote is setting up remote to fail. It states that remote will only work if multiple people feel change that is needed to make remote work.

**Conclusion**

I have seen my fair share of "old way" thinking while working traditional and nontraditional jobs. It pains me to see this "old way" still strong in management today. Both of these books push the idea that there is a better way. It is a new and different way, there are pitfalls, but in the end you will be happier, your employees will be happier, and your product will be better. I highly recommend these books to pretty much anyone, exceptionally if you are working in a creative job such as development or design.