---
path: "/2010/10/06/android-development-the-install/"
date: "2010-10-07T02:30:17Z"
title: "Android Development: The Install"
categories: ["General", "Project Updates"]
tags: ["Android", "Programming"]
excerpt: "I recently picked up my first smart phone. After years of wanting to move up to a smartphone but ho..."
---

I recently picked up my first smart phone. After years of wanting to move up to a smartphone but holding off because of price, I finally caved and picked up a HTC Desire. It is running Android 2.1 out of the box. It is a pretty slick phone. So far I have enjoyed my prechuse.

One of the reason I decided to pull the trigger was the idea of creating application for the phone. By day I am a .NET developer. Mainly C#. I was sort of excited to learn a new development environment and langue because I have been in .NET land for so long. I figure it is time to branch out. Yeah java is not much different than .NET but you have to start somewhere. :)

So I had some time to get the Eclipse IDE setup and ready to go to start my journey. I went to [http://developer.android.com](http://developer.android.com/) and followed the instructions on getting started. I am starting from scratch. The machine (win7) I am working on didn't even have the Java runtime installed yet. Since I am a novice when it comes to Java and Eclipse, I followed the installation steps for the Android SDK. They seemed straight forward.

First step: Get eclipse. Sounds easy enough. Read through the section for ecplise which stated...

> A Java or RCP version of Eclipse is recommended. For Eclipse 3.5, the "Eclipse Classic" version is recommended.

What does that even mean? So I want to the Eclipse site and downloaded the Java version of Eclipse 3.6 and continued to the section to install the Android Develipment Tools (ADT).  This is where things get more confusing because I was greated with...

> **Caution:** There are known issues with the ADT plugin running with Eclipse 3.6\. Please stay on 3.5 until further notice.

Grrrrrr.... But I just downloaded 3.6.1!! To limit the number of issues I run into I decide to follow what Google recommends and found a copy of Eclipse 3.5\. No big deal. I go to run Eclipse and it promptly fails stating it can not find the JVM. At this point I was confused because I had thought that I installed Java at some point in the past. Yeah, I was wrong. Whoops. Over to the Oracle to get the JRE/JDK.

I am running win7 x64 and did what anyone running x64bit OS would do. I download the x64 windows binary. Install Java. Run eclipse. Eclipse fails again. Still stating that it could not find the JVM. Ugh. The message box suggest to add the path to the JVM to my system path. A few clicks later and it should be good to go right? Nope, now Eclipse crashs but with a log/debug message.

At this point I was about ready to give up. That is until it dawned on me. "Hey stupid! You probably can't run 32bit Eclipse with 64bit JRE!" An uninstall and reinstall of the JRE and eclipse is running!

From here on out everything seem to work correctly. Totally elapsed time: ~2 hours. No really 2 hours.

My enthusiasm has lessened alot because of the frustrating install. I mean I have not even started to write a hello world Android app yet. I am planning on sharing my experience as I go and comparing it to how I view the .NET start up experience.