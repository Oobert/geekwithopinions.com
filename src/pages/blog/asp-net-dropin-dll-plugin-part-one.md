---
path: "/2013/06/12/asp-net-dropin-dll-plugin-part-one/"
date: "2013-06-13T01:02:15Z"
title: "ASP.NET Dropin DLL Plugin - Part One"
categories: ["Project Updates"]
tags: ["ASP.NET MVC", "C#", "Programming"]
excerpt: "My intro to web programming was with PHP and scripts such as phpBB and other internet forum softwar..."
---

My intro to web programming was with PHP and scripts such as phpBB and other internet forum software. I always like that installing a plugin normally just worked. You would drop a folder into some directory and the script would see it and you could install it. I have yet to really see this recreated in ASP.NET MVC.

For the past year or so I have been trying to find a way to recreate this. There are many blog posts and half tutorials on this. There is even a few libraries out there on how to do this.

[And old blog post from 2008](http://www.wynia.org/wordpress/2008/12/aspnet-mvc-plugins "MVC Plugins") was my starting off point. It talked about Virtual Paths. It worked but the syntax was terrible.

I knew if I was going to do this, I wanted everything to just work as easily as possible. I also wanted the plugins to be runnable on there own if the developer wanted (for dev and debug reasons).

[Griffin.MvcContrib](http://blog.gauffin.org/2012/05/griffin-mvccontrib-the-plugin-system/#.Ubm9HPmfh3A "Griffin MVContrib plugin") has a way to do this. It seems overly complicated to me. So I kept at it.

After some more searching I found an example of some code that implemented a virtual path provider. I don't know from where or who but I feel bad because I would like to give them credit. I finally had enough to start tinkering on my own.

I finally have something to show for all of this. I have [posted on github](https://github.com/Oobert/ASP.NET-MVC-Plugins "github link to project") a working example of how to create a ASP.NET MVC plugin system. This system allows for DLLs that contain all necessary files, compiled or embedded, to be dropped into the bin directory of a main site and then those files served from the DLL.

Over the course of a few more post, I will cover how the important bits work and what the gotchas are. If you need this now, check out the [github project](https://github.com/Oobert/ASP.NET-MVC-Plugins "github project"). Currently, it has one plugin that has JavaScript and image examples.

Questions are always welcome.

[ASP.NET Dropin DLL Plugin – Part Two](http://www.geekwithopinions.com/2013/07/09/asp-net-dropin-dll-plugin-part-two/ "Edit “ASP.NET Dropin DLL Plugin – Part Two”")