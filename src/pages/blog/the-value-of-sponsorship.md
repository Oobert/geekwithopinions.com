---
path: "/2019/04/10/the-value-of-sponsorship/"
date: "2019-04-10T17:00:00Z"
title: "The Value of Sponsorship (Updated)"
categories: [""]
tags: [""]
excerpt: ""
---

Sponsorship is a misunderstood thing. Many businesses don't understand the value that sponsoring events can bring them. I get why though; the value is really hard to measure. In this post, we are going work through the value that comes from sponsorship. 

It should be said ahead of time, I ran the [Milwaukee JS meetup for 5 years](https://www.geekwithopinions.com/2019/03/26/five-years-of-milwaukeejs) and have been a fan of [ThatConference](https://thatconference.com) since its inception. Also, I have my employers sponsor events over the years. Or to put it more simply, I am a community advocate. Now that my bias is clear, let's continue.

### Recruitment
Recruitment is normally the go to reason to sponsor an event. Businesses will want to know the demographic of who attends and how they should interact with the attendees at the event. Most importantly, they want to know if they will get access to the attendees' info. Essentially, the business is building out its network of possible applicants for positions now or in the future. The end goal here is to make hiring easier. The more people in a business's talent pool, the easier it is to find talent. 

### Sales and Marketing
Sales and Marketing is the next big reason to sponsor an event. It is a straight forward to get a product or service in front of a business's target demographic. Make sense, as a business will go where their customers are. In this case, you see a lot of software developer tooling and services at tech conferences. Microsoft is known for their tactics in this space. Basically, their goal is to get the individual hooked on their product by giving away free versions and hope that individual takes it back to their work to advocate for its use. 

These two reasons for sponsorship are the most obvious reasons and also easiest to measure. You can easily put metrics around how much was sold, how many referral links were used, how many leads were generated, and how many people where hired. These tend to be the only focus for most places. 

But is that it? Is that the only reason to sponsor events? Is there more? Well, of course or else this would be a really short blog post.

### Direct Value Add
People who go to events, go to learn and/or grow their personal network. They then take this personal growth back to their job and apply it to their work on (best case scenario). This means the employee's employer gets direct value from the event having happened. They get access to the knowledge through what was presented at the event. This can show up in all sorts of ways like better solutions, leads on talent to hire, and/or better preforming employees.

I have said that my community advocate work is largely selfish. I want Milwaukee's community to be bigger and brighter because I want to learn from the people in the community that have knowledge I don't. 

This is what I am calling direct value add (mainly because I am terrible at naming things). You can trace the knowledge directly back to when the employee learned it. 

### Indirect Value Add
Now a deeper question comes up. Does a business get value just by the very existences of an event? For me, that is an emphatic yes! A vibrant community will draw people to that community. If this is a local community, it means both the talent and knowledge pool grow. A business can indirectly benefit from this growth. There isn't a great tie back to anything tangible here. For example, it is hard, near impossible, to say that Grace wouldn't have been hired if the JavaScript Meetup didn't exist. 

However, if you talk to those who have moved to Silicon Valley, SF, or NYC, they may state the vibrant tech communities as a reason for moving across the country. People generally want to be around other people that have something to offer them like knowledge, motivation, or jobs.

Once again, I am bad at naming thing, so I am calling this Indirect Value Add. It gets rough to trace this back to a single event happening.

---

I can't say that Direct and Indirect value add is somehow more valuable reasons to sponsor an event. There just isn't stats to support that claim. With that said, I would argue that these are very acceptable reasons to sponsor an event. If a business is getting value from an event existing for whatever reason, it would then be in the business's best interest to make sure that event continues to exist. This doesn't apply to just local events either. Maybe you are a JavaScript shop and send people to JS Conf each year. If the employees are bringing value back from the conference, it would be in the employer's interest to support this event. This also applies to open source software but that is another post.

With all that said, a business should figure out what events bring them the most value and support those. As much as I wish every event had a ton of sponsors, that just isn't possible or reasonable.

---

Updated (4-24-2019)

The above was posted a few weeks ago and based on feedback, I missed a few thoughts above. So, they are going to get added here now.

### Retention
Really? Sponsoring an event can help retain employees? It turns out that employees have a more positive opinion of their employer when they support the community they work in. The tech community in this case. Speaking from experience, there is a warm fuzzy feeling, pride maybe, when you attend an event and your employer is there as a sponsor. But how? Why? It comes down to actions are louder than words. An employer can say they care about the tech community, but without actions those are just words. By taking an action (sponsoring an event), it is showing that the company has an active interest in the community.

### Philanthropic
It seems employees have started to want the companies they work for to have morals and ethics. What a thought... They even want to see their employer doing social good or have good [corporate social responsibility](http://freakonomics.com/podcast/corporate-social-responsibility/). This idea is very similar to the retention idea above. Employees like the fact their company is sponsoring (supporting) events they care about including events outside their industry. Some examples would be supporting community cleanup efforts, local charities, or cancer foundations. Anything that is for the "greater social good" falls into this bucket. 

Thank you [Greg Levenhagen](https://twitter.com/GregLevenhagen) and [Pete Prodoehl](https://twitter.com/raster) for the suggestions!

