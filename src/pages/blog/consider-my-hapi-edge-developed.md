---
path: "/2015/08/25/consider-my-hapi-edge-developed/"
date: "2015-08-26T02:49:27Z"
title: "Consider my Hapi Edge Developed"
categories: ["Book Club"]
tags: ["hapijs", "JavaScript", "nodejs"]
excerpt: "The nice people over at Bleeding Edge Press noticed that I submitted a pull request to hapijs relat..."
---

The nice people over at [Bleeding Edge Press](https://twitter.com/edgepress) noticed that I submitted a pull request to [hapijs](http://hapijs.com/) related repos at some point and asked if I would be interested in reviewing a book about [hapi](http://hapijs.com/). Being a fan and apparently wanting to pushing myself I said sure. A few hours later a copy of ["Developing a Hapi Edge"](http://bit.ly/1Wa4ely) by [Van Nguyen](https://twitter.com/thegoleffect), [Daniel Bretoi](https://twitter.com/eydaimon), [Wyatt Preul](https://twitter.com/wpreul), and [Lloyd Benson](https://twitter.com/LloydWith2Ls) was in my inbox. So let's do this!

A quick elevator pitch on hapi. It is ["a rich framework for building applications and services."](http://hapijs.com) In non-marketing speak it is a framework for building applications for the web. It is exceptionally good at restful APIs and best known for being the framework that [powers parts of Walmart](https://twitter.com/hashtag/nodebf). Personally, I like it because it just gets out of my way. In fact this very site is served through a hapi proxy.

Over the past few days I read through the book. I am going to start of with what this book isn't. It isn't the book you should buy as your first node book. This book makes the assumption that you know node, have an understanding of the web, and jumps right into hapi. This is a good thing because the book can focus on hapi.

When I say the book jumps right in, I am not kidding. Chapter 1 is a brief history and overview of the book. Chapter 2 turn up the speed. It starts off with setting up a simple server and revs up to different tools that you can use to setup and run you hapi application. This pace is continue throughout the book. I like the pace because it never stays on any topic to long. About the time I am getting tired with a topic the book moves onto the next one.

At the pace of the book, you may get overwhelmed with all the info coming at you. Trust me, by the end of the book it all comes together and it will all make sense. The authors make up for the pace by having a full blown application that book is based on.

[Hapi-plugins.com](https://hapi-plugins.com/) is the project this book is based on and each chaptor shows how a different part of the hapi infrastucure was built. The best part about this project is the code is available on github. It makes it really easy to see what the book is talking about in a real web application which helps in the learning of hapi. Other technical books or blogs have just shown code demos that you would not use in a real application. Having this project a long side the book was refreshing because if feels like the authors really wanted to show real world use of hapi instead of just some demos.

The authors even took the time to go over debugging and security. While these chapters are by no means a complete resource, it was nice to see a brief overview on the topics. It was enough to get your started with debugging and to get started at developing a secure website. Like everything else in the book, there was little fluff and was right to the point.

So is this book right for you? If you are looking at a quick and short guide to learn hapi, on a team that is using or about to use hapi, or a technical book worm, then ["Developing a Hapi Edge"](http://bit.ly/1Wa4ely) is a good buy for you. If you are looking for a reference book, this is not really it nor does this book claim to be that. The hapi documentation is really well done and an excellent reference.