---
path: "/2009/05/16/windows-7ie8-download-feature/"
date: "2009-05-16T14:59:34Z"
title: "Windows 7 Feature"
categories: ["General"]
tags: []
excerpt: "So I was getting my oil changed today. While I waited I took advantage of the free WiFi. I had to d..."
---

So I was getting my oil changed today. While I waited I took advantage of the free WiFi. I had to download Adobe Reader because of the new install of Windows 7\. Below is a screenshot of one of the new features of the task bar. It appears applications can change the button on the task bar. The screenshot is of the progress bar for a download in IE8\. I think it is pretty cool and will be more useful than flashing that task bar buttons to get my attention.

![Progress bar](16-1.png)