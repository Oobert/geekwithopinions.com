---
path: "/2017/03/02/time-for-a-change-part-2/"
date: "2017-03-02T13:55:48Z"
title: "Time for a change, part 2"
categories: ["Community"]
tags: ["Career", "Community"]
excerpt: "This the 2nd part in a series of posts about improving the dev culture in the Milwaukee and surroun..."
---

This the 2nd part in a series of posts about improving the dev culture in the Milwaukee and surrounding area. See all the post in this series under the [community tag](http://www.geekwithopinions.com/tag/community/).

In the previous post, I told a story of Sally. She is a passionate developer that is getting frustrated with the status quo in Milwaukee. The post was mainly aimed at companies in the area. This post is aimed more at the developers in the area. The overall feedback from the previous post was pretty positive however there was one common comment that come up from story of Sally. The common comment was "how did Sally attempt to change her company?" This question is what we are going to explore in this post.

* * *

Change is very hard for most people. In fact, I would argue that change is a fear that is right with public speaking and death for many people but different in that many don't know they are fearful of change. Change can be risky, Change is unknown, Change can move you from your comfy safe box.

The first question from the story of Sally that should be asked is did she have the support from the business for the changes she wanted to make? Any change is easier with support from others. The higher up the organization the support goes, the easier it gets to affect change in an organization. The best case scenario here is if c-level execs are leading and/or cheerleading the effort. Since Sally was thinking of moving out of Wisconsin, it is safe to assume she didn't have much support in this way.

The next question is then what has she done to drum up support? More importantly, how? Many developers' first instinct is to try to win others over by using the thing they know best: their technical knowledge. Which is great when talking to other technically minded people. Most of the time. It falls flat if you are trying to convince non-technical people.

Let's assume that the change(s) Sally wanted to make were valid and would in fact be beneficial to the team and company. What would she need to do? She would need to get her ducks in a row. When she presents her ideas to her audience, she needs to be able to show them these changes will positively affect the things her audience cares about. She needs to show that she has thought through changes and be willing to get answers for the questions that come up. Even better, know her audience well enough to know the questions they will want answered and have them answered ahead of time.

There are 3 outcomes to this. The change is agreed to, flat our refused, or declined/deferred with reason.

If the change is agreed to. Make sure it succeeds. There will be trust and credibility to be gained. This can be used for further changes down the road.

If the change is flat our refused, well that sucks. These are hard to take. Many devs crave to have answers to the question of "why". Not having reasons will likely irritate a dev. If you get too many of these answers, it may be a sign to move on (which is a totally different topic).

If the change was refused or deferred with reasoning provided. This is where understanding is needed. Many developers don't like to be told they can't do they thing they wanted to do. I know it is more fun to be upset then to find understanding. However, there are lots of reasons why a suggested change would get shot down. Many of them are very valid. A solid example is software licensing. You can't just take some piece of software or code off the internet and use it if it is licensed wrong. No matter how much it would help. Hell, tons of companies shy away from Reactjs because of the inclusion of a patent grant. But I digress. Point being there is a lot to be learned here. A lot that can be used in further discussions either on this change or future changes down the road.

The story of Sally is just made up but has its roots in truth. I have been the developer that just spews technical lingo at a director only to be frustrated when he or she says no. I have watched my peers do the same thing. I have even seen a few of them get so frustrated they moved out of Wisconsin. On the flip side, I have seen great changes happen and it is exciting.

Look, none of this is easy. If it was, I wouldn't be writing this. And this has everything to do with making Milwaukee and Wisconsin a better place for developers. It is going to take work on our part to do this. It is going to take stepping out of our comfort zones and making the places we work better. I know there is a ton of nuance here and there have been whole books on "[How to Win Friends and Influence People](https://en.wikipedia.org/wiki/How_to_Win_Friends_and_Influence_People)" written. This is just a start. Just me saying that we need more developers to stop accepting the status quo and work to make our hometown a better place for everyone.