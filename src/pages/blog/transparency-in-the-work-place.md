---
path: "/2013/12/12/transparency-in-the-work-place/"
date: "2013-12-12T19:59:37Z"
title: "Transparency in the Work Place"
categories: ["General"]
tags: ["Career", "life"]
excerpt: "Image a CEO walks in one morning and states loudly for everyone to hear \"The release date has chan..."
---

Image a CEO walks in one morning and states loudly for everyone to hear "The release date has changed from 5 weeks to 2 weeks. Everything must be done." and walks away.

The first questions from everyone is: What just happened? Why did the date move? How are we going to finish this 3 weeks early? Productivity will remain very low until answers arrive or the shock wears off. And the rumors will start. Maybe we have a client? Maybe we are being sold? Maybe we ran out of money? Maybe the CEO is a bitch?

Many of us of lived through this example or examples like it far too many times. Thinking back to the times this has happened to me, the majority of the problem wasn't with the information I was receiving. It was with the number of questions it created. My must crippling one was Why. I, like many, will spend extremely too much time trying to understand  why changes was made or why something works

As developers, a large part of our day is understanding the whys of our software. Why does it work in this case but not that one? Why does this user click a button 5 times? Why did bob eat that? To many of us, not knowing why is like having an itch we can't scratch. It will plague our minds until we have a suitable answer. This is also what makes us good programmers but that another post.

Transparency can solve this and so much more. [Forbes agrees](http://www.forbes.com/sites/glennllopis/2012/09/10/5-powerful-things-happen-when-a-leader-is-transparent/3/ "Transparency"). There are many benefits to being transparent but the one I am most interested in is the one that bugs me the most. Answering the question of why.

Looking back to the example, if the CEO was completely transparent, good or bad, it would have allow the staff to cut through the crap and get to the point. The deadline was moved because there is a huge opportunity for the company if we can hit it. Or the deadline was moved because if we are not done in 2 weeks, we are going to run out of money and everyone is laid off. In either case, why was answered and the staff can move on to dealing with other questions like how.

I have been more loyal and understanding to a boss that was transparent even when the information was bad. I knew that they were telling me all they knew and I understood their choices more completely, and was willing to follow their direction more often.

With a boss that was less than transparent, I have been more questioning of their motives and if they really had the teams best interest in mind.

I am not alone with this way of thinking. Many of the my co-workers over the years exhibited the same tendencies.

Statements like "Something bad is happening. Why would we do that know? It doesn't make sense." are common place when transparency is limited. My suggests to the management of the world is to treat us like adults. We can handle bad news. If an employee can't, you probably didn't want them as an employee anyways.