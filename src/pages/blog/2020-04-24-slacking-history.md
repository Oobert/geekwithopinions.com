---
path: "/2020/04/24/slacking-history"
date: "2020-04-24T15:01:27Z"
title: "Snippets from Slack: On Tech History"
categories: [""]
tags: ["Slack-ing"]
excerpt: ""
---

Embrace the current and future. Understand the past. Tech has a habit of running in circles. What is old becomes new again. It is mainly because hardly anyone takes the time to learn where tech has been, the problems that already have solutions. We see a new to us problem and go “I got this!“… instead of seeing that it has been solved by people way smarter than us.

I am not suggesting we blindly follow the past. I am suggesting we learn from it and improve on it instead of recreating the past.