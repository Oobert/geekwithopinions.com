---
path: "/2016/12/22/time-for-a-change/"
date: "2016-12-23T03:16:19Z"
title: "Time for a change"
categories: ["Community"]
tags: ["Career", "Community", "Meetup"]
excerpt: "Most of the people who know me and are reading this probably know me from my work with the Milwauke..."
---

Most of the people who know me and are reading this probably know me from my work with the Milwaukee JS meetup. Anyone that has had a conversation with me at the meetup has probably heard me talk about making Milwaukee and Wisconsin better. Specifically for tech but overall as well. This is a subject that I am passionate about and is one of the key reasons I continue to put effort into Milwaukee JS.

Honestly, it doesn't matter who I am. The only thing that matters is that you read this and start to give a damn. It is time for the people here care. For the companies here to care. It is time for real change not just saying words but actually doing something that will better all of us.

Time for a story. Sally is a software developer that works for you or with you. She is a good developer, gets her work done and is currently contemplating moving out of the state to pursue better jobs. This seems crazy right? I mean Sally's job pays decent. Her company is stable. Why would she risk everything to move 2,000 miles?

The answer? Many people and companies don't want to hear the answer. Or they don't want to believe the answer. The answer is because Sally's job consists of writing code that is outdated, boring, and is the bare minimum the company needs. She spends her nights learning whatever she can to become a better developer but all attempts to use what she has learned at her company have failed. So she is willing to risk everything, move 2,000 miles just for a chance to use more of the skills she has accumulated.

Many of you are thinking this story is just that; A story. Well, I hate to break the news to you it isn't. I have at least 8 friends and acquaintances that have moved for this very reason. And more that are considering a move and they probably work for you or with you right now. Most them could have been considered "community leaders". They ran/run meetups and tried to make an impact at their respective places of employment only to have it not go anywhere.

Need more proof there is an issue? Go to a meetup. Any meetup. Talk to the developers that are there. They will tell you the struggles are real. They will tell you that it is hard to find places to work in the area that actually give a damn about development. They will tell you that it has crossed their mind at least once to move 2,000 miles to a place where more people give a damn.

Still need more? Fine. Walk down to your HR/talent acquisition people and ask them. Ask them how much work they have to do to find a hireable person. They will tell you, that in Milwaukee it is really hard to attract talent.

Ok so there is a problem. So What can I do about it?

As a company let your developers participate in the developer community of Milwaukee and Wisconsin. On company time. Developers have lives outside of work but many of us would love the chance to show off what they do at work. They should not be expected to do this on their time. They should be doing it on yours as it will benefit you. So let them. Not only will this net more engaged developers but it will allow your company to shine in the community which in turn will help attract more talent.

I bet the majority of our small community in Milwaukee don't know the cool and interesting problems that are being solved in your four walls. Hell, I would bet some of the employees inside your four walls don't know the extent of interesting problems that are being worked on within your four walls.

So I beg you. Let them speak. Let them blog. Let them be more open on the awesome you are doing and the problems you are solving.

As a developer in this community, I encourage you to step outside your comfort zone and push your employer to allow you to contribute to the community. This can be blogging, speaking, tweeting or whatever. I for one believe that we, as developers, are the best advocates for our employers to other developers.

At this point you maybe asking yourself why does this person care so much about a company he doesn't work for. Why does he care about what I do?

The answer is simple. I am selfish. As the quality of developer jobs in Milwaukee improves, we will attract more talent. As more talent comes to Milwaukee, I (we) can all learn more from our peers. The more I (we) learn from our peers, the better I (we) become. To put it another way, I want smart people around me because it motivates me to be smarter, better, faster.

* * *

Full disclosure: I organize the speakers for the Milwaukee JavaScript Meetup. MKEJS has over 1100 members. It is very hard to find speakers. After a fear of public speaking, the next top excuses are "I am not doing anything interesting", "I don't do anything on the side to talk about" and "My company won't ok me speaking" All of these point to a problem in our community. All of these are fixable.

So this post is just one step in my work to make an impact. I need more and different speakers to keep MKEJS going. To keep it interesting. I want speakers from every company, every background, race, and gender. I am asking for help in making this happen not only for MKEJS but for every meetup in Milwaukee.