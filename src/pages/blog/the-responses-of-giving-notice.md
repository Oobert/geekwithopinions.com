---
path: "/2015/01/14/the-responses-of-giving-notice/"
date: "2015-01-14T17:31:22Z"
title: "The responses of giving notice."
categories: ["General"]
tags: ["Career", "life"]
excerpt: "This morning is the day. After 6 years with the company, Tom has decided it was time to move and is..."
---

This morning is the day. After 6 years with the company, Tom has decided it was time to move and is putting in his notice. He is nervous and excited. Tom meets with his direct manager and lets him know. Because of their one on ones Tom's manager is not surprised and saw it coming.

Later, Tom is in the teams weekly update meeting and breaks the news. It is a rough moment because he has spent the last 6 years with this team and now he was leaving. In that moment, everything is now different.

Jen has worked with Tom for many years. They have become friends and colleagues. She is understanding of why Tom has decided to leave and is supportive. She knows that they will stay in contact long after Tom has left the company.

Hugo is the VP of Development and has been with the company since forever. Hugo is extremely upset. He views Tom's choice as an insult to him and the company. How could someone just leave? Hugo will make it known of his anger to Tom, typically by being passive aggressive. Making comments about loyalty and trust.

Adrian is the Project manager. She is scared. Tom was a big part of the team and with him gone, she is unsure on how they are going to fill in the gaps.

John, another developer on the team, is sad. Tom was a mentor to him and now that he leaving, it's like a peice of John is now missing. John is visibly upset over the news and anytime the topic of Tom leaving comes up, the emotions return.

Taylor, a mid level developer, is excited. With Tom leaving, this means there is a chance to step up and get noticed. She is pretty much pushing Tom out the door so she can start taking on more responsibility and start showing her worth.

When a person puts in their notice, everyone affected has one or more of the above reactions to some degree. In my experience, the majority of people will be like Jen. They will be understanding and supportive, maybe a tiny bit sad. They understand that the action of the person leaving is not directed at them nor some insult. The unexpected outliers that have a more extreme reaction are the ones to look out for as these are the people that make leaving a company harder than it should.

The bright side of it all is that these feelings will pass. Adrian will see that people like Taylor step up and fill the gaps. John will realise that Tom was a great mentor and has prepared him well. Even Hugo will get over it as anger is sometimes just a defence mechanism.

When you put in your notice, be ready for the sudden and abrupt change in how your soon to be ex-co workers interact with you. As you tie up loose ends, you will slowly fade to the background. Don't fret though, you are about to start something new.