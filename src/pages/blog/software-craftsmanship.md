---
path: "/2014/01/03/software-craftsmanship/"
date: "2014-01-03T16:32:50Z"
title: "Software Craftsmanship"
categories: ["General"]
tags: ["Career", "Programming"]
excerpt: "On my way home from work the other day, I was listening to .NET Rocks Episode 934with Uncle Bob on ..."
---

On my way home from work the other day, I was listening to [.NET Rocks Episode 934](http://www.dotnetrocks.com/default.aspx?showNum=934 ".NET Rocks: On building software with Uncle Bob")with Uncle Bob on building software. [Carl,](https://twitter.com/carlfranklin "carl franklin") [Richard](https://twitter.com/richcampbell "richard campbell"), and [Uncle Bob](https://twitter.com/unclebobmartin "Uncle Bob") had a discussion on [HealthCare.gov](https://www.healthcare.gov/ "healthcare.gov") and the issues with the site from a development stand point. At 28 mins 45 seconds in , Richard states "I am responsible to my industry first and my customers second" and this struck me as very profound.

I have never considered idea before that my actions, code, and software is a representation of my industry. That because of my actions as a developer could causes a person to view all programmers in a different way.

If we look at lawyers for example, we can see the stigma of the job. Society seems to have negative view of lawyers. That somehow you are a terrible person if you go into law. Why is this? There are terrific people that work in law. I have met them and worked with them. The negative view was probably built by the few that have acted unprofessional. The ambulance chasers and those who make frivolous lawsuit just to make a buck. My point, is that it won't take much for our profession, software development, to get a similar stigma if projects keep failing.

I fear that the stigma of a software developer not being professional, not caring is already taking hold. The software the world uses every day is pretty terrible. Why is my password limited to 12 characters for my online credit card account. Why does the family doctor tell me all the issues he has with the EMR instead of telling me how awesome it is? Why does my cable box freeze so much? Why does Google Chrome's spell check not fix the fucking word when I click on it? People should be excited about how software is making their lives easier not about how much it sucks. Our jobs as developers is to provide software that helps people not infuriates them.

<span style="line-height: 1.5em;">Uncle Bob and many others created the [Software Craftsmanship](http://manifesto.softwarecraftsmanship.org/ "software craftsmanship manifesto") manifesto in 2009\. The goal of this movement is to push craftsmanship and professionalism in the industry. The general idea is to promote doing what is right over getting it done. Good enough is no longer acceptable. </span>

<pre>Not only working software, but also well-crafted software
Not only responding to change, but also steadily adding value
Not only individuals and interactions, but also a community of professionals
Not only customer collaboration, but also productive partnerships</pre>

I have signed the manifesto as a reminder to push my industry forward. To not sit idly by. To make awesome!