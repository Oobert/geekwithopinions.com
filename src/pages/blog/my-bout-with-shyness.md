---
path: "/2014/10/16/my-bout-with-shyness/"
date: "2014-10-16T13:40:59Z"
title: "My bout with Shyness"
categories: ["General"]
tags: ["Career", "life", "Soft Skills"]
excerpt: "I am shy. I am very lucky that it is not crippling. But it is bad enough that it has and is keeping..."
---

I am shy. I am very lucky that it is not crippling. But it is bad enough that it has and is keeping me from doing what I really want to be doing. For example It was keeping me from being active in the development community. I would like to be able to introduce my self to people I want to meet when I see them. Mainly I would like to stop being anxious when I meet new people.

It is weird because I have not been able to figure out if I am an introvert or extrovert. I have traits from both sides. I love talking in groups of people I know but also love the times I off doing things on my own. Communications is one of my top 5 strengths from [Strengthsfinder](http://www.strengthsfinder.com/home.aspx "Strengths finder"). I don't know if it is possible but I feel that I am both but on different days.

Now my shyness is really around people I don't know or don't know well. If I don't know you, I am not comfortable around you. I can not just walk up and start a conversation. It causes me large amount anxiety. I would go to meetups and conferences and talk to no one while I was there. It bugged me a lot. That is until I decided enough was enough and that it was time to do something about it.

3 years ago, I attended the first [That Conference](https://www.thatconference.com/ "That Conference"). I love learning, it was inexpensive, and close by. I knew exactly 1 other person that was attending. I was excited. That was until morning of day 1.

Day 1 started with Clark Sell standing in front of the attendees and issuing a challenge. He stated that That Conference was designed to be a social conference. Yes the session are awesome but the organizers put ample time between sessions and had after hours events to foster socialization among the attendees. His challenge was to take these opportunities to meet new people.

Pretty much after that moment, I was freaked out. I was thinking I can't do this. I am going to be that guy sitting in the corner by himself for 3 days. For the most part of day 1 that was true. Thankfully the 1 other person I knew is more outgoing than I am and found a group of people to hang out with on night 1\. If it wasn't for that 1 person, I would have been hiding in a corner for all 3 days.

To be clear, I love That Conference. I love, now, that it is setup to be social. And this challenge was exactly what I needed but I didn't know it at the time.

After the conference was over, I was inspired to do awesome things. During the following months, I decided it was time to get over my shyness or at least deal with it. I didn't really have a plan yet but I had an idea of what I wanted to do. I didn't want to be the person in the corner anymore. I started off by attending more meetups. At the very least I was getting more comfortable with being around people I didn't know.

About a month before the 2nd That Conference, I got it in my head that was going to do an open spaces with the topic of developers with disabilities. I am dyslexic, this was my in. After the 3 days, I ended up chickening out. Fear sucks. The whole drive home, I regretted not doing it. The feeling sucked even more than the fear.

All was not lost, one amazing thing happened during the conference. During the time between the 1st and 2nd year, I became a bit more active on Twitter. While at That Conference, Clark Sell sort of called me out for not saying hi. I was hiding behind twitter and he basically said stop it. This sort of forced me into doing something I am not comfortable with. That was getting out of my chair and saying hi. I did it. It was a small victory. It was a step toward the realisation that getting over this is possible.

At this point it is August 2013 and it was time to get serious. I set 2 goal that I wanted to complete. Become active in development community and host an open spaces at That Conference 2014.

About this time, I learned that Milwaukee's development community had an IRC channel (#devmke on freenode). I jumped in and idled. Even with just text between me and others I had anxiety about joining in the conversations. Over time, I learned who was who and started to join in. Slowly. Because of Twitter and IRC, I got to follow and interact a bit with people that organized some of the meetups in Milwaukee.

In early 2014, the organizers of [MKEJS](http://www.meetup.com/milwaukeejs/ "MKEJS")were talking in IRC about the need for a topic/speaker for that month. I suggested that they do a [Node School](http://nodeschool.io/ "Node School") workshop. I am not sure how but my suggestion turned into me facilitating the workshop. Node School workshops are a set of self guided lessons that use node to teach you node and other aspects of JavaScript. My job facilitator would be to get people started, show them how to use it, and help them if they get stuck. On top of this, this was looking to be the most popular MKEJS meetup yet.

Facilitating this workshop was an great step forward. They say facing your fears is a good way to get over them. Well public speaking to a group of people I don't know. What could possibly go wrong? I faced them head on and everything went really well and gave me the confidence to do more. Shortly after, I was asked to help co-organize the MKEJS meetup which I accepted and at roughly the same time joined the Content Advisory Board for That Conference.

This August was the 3rd That Conference. So far I have reached one of my goals and have become more active in the development community. Now it was time to complete goal number 2 and do an open spaces. Before the conference, I told a few people my plan and asked them to help me make sure I do it. Since I didn't went to let them or myself down I completed the goal. [Here I am 3rd in line](https://www.flickr.com/photos/thatconference/15027300917/in/set-72157647101686058 "Open spaces picture.")to tell my topic to roughly 1000 other developers, most I have never met. Yes I was nervous. Yes, stumbled a bit but I did it.

The conference went amazingly. I was able to meet many people I know from IRC and twitter in person for the first time. I was uneasy about approaching complete strangers but did so on a few occasions to recruit speakers for MKEJS.

Since That Conference this year, I had a new goal of giving a talk. This goal was completed in September when I gave a talk Titled "[Rocking with Web Audio API](http://www.geekwithopinions.com/files/WebAudioTalk/index.html#/1 "Slides")" to MKEJS.  The next goal is to give a talk at That Conference next year.

I am still a bit shy and still have problems approaching people but I am going to continue to work at it. It has been a slow process and I think it had to be for me. I had to be ready for the change to happen and that takes time.

As I was writing this, I realised that a lot of my anxiety stemmed from not having confidence in myself. The reason this took time is that I needed to build up my confidence before I could do the next thing. Now to work on that talk for That Conference 2015.