---
path: "/2011/02/25/book-review-being-geek-the-software-developers-career-handbook/"
date: "2011-02-26T03:50:27Z"
title: "Book Review: Being Geek - The Software Developer's Career Handbook."
categories: ["Book Club"]
tags: ["Books", "life", "Programming"]
excerpt: "I'll admit, I am not much for book reading. I like skimming the Internet and blogs but setting down..."
---

I'll admit, I am not much for book reading. I like skimming the Internet and blogs but setting down with a book is not my thing. Maybe it is my Dyslexia, who knows. That is why I was surprised on how fast I read [Being Geek](http://beinggeek.com/ "Being Geek") by [Michael Lopp](http://randsinrepose.com/ "Michael Lopp").

Michael, who often goes by the name Rands, is a engineer manager in Silicon Vally by day and writer/blogger at night. Besides writing books, Rands writes a blog called [Rands In Repose](http://www.randsinrepose.com/) where he gives advice about living, working, and dealing with geeks and being a geek. It is well written blog with good content.

Back to <span style="text-decoration: underline;">Being Geek</span>. The book claims to be the "The Software Developer's Career Handbook" and it does just that. The content is not earth shattering but it does put things into perspective. <span style="text-decoration: underline;">Being Geek</span> is written for the target audience of Geeks who tend to view the world in black and white. The content flows from the start of a Geek's career to the end the current gig along with everything in between.  Chapters are short and to the point, each has a point to be made. While connected and in an order, the chapters can stand on their own. This made the book a joy to read because I didn't have to go through the fluff to get to the point.

I thought the book was over all very good. There were a few chapters that felt they didn't apply to me at this moment. These parts where more about being a manager, which I am not (yet anyways). With that said, may of the chapters did apply. The first section of the book walks through the finding a job and interviewing process. This was probably the biggest eye opener to me. I did not realise all the minuscule detail that interviewer is looking for, that is if they are any good. Rands goes into detail on what to expect and what the goals of the interviewee should be. For example, the phone screen, I learned, is all about communication. Can communication flow between the interviewer and interviewee. Rands also discusses the type of questions the interviewer is probably going to ask, why they ask it, and high level suggestions for answers.

In the end the book gave a lot of good information in a no nonsense cut the crap kind of way. I give credit to the Book and Rands for giving me some confidence and motivation in my career.