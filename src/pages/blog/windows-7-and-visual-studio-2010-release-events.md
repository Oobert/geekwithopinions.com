---
path: "/2009/06/01/windows-7-and-visual-studio-2010-release-events/"
date: "2009-06-01T12:17:59Z"
title: "Windows 7 and Visual Studio 2010 Release Events"
categories: ["Rumor Mill"]
tags: ["Microsoft"]
excerpt: "Windows 7 is due out by the end of this year. Visual Studio 2010 is due out next year. Does this al..."
---

Windows 7 is due out by the end of this year. Visual Studio 2010 is due out next year. Does this all sound a bit familiar? It should because there was a similar situation with the release of Windows Vista and Visual Studio 2008\. I have always liked going to Microsoft events and drinking the Microsoft "Kool-Aid". It makes me feel all warm and fuzzy inside! We all assume that there will be events for Windows 7 and Visual Studio 2010 because Microsoft has a track record having events for their major releases. Well, it is my understanding that this time is no different.

At a recent developer event, I happen to catch up with a few developers from Microsoft and asked just what is going on. Here is what I know. The Windows 7 and Visual Studio 2010 events are in the planning stages at Microsoft. They will more than likely be like the "Heroes happen {here}" events for Visual Studio 2008 and Windows Vista. And lastly, the event will more than likely happen next year.

Nothing I was told is set in stone and is all likely to change 10 times in typical Microsoft fashion. And just so there is no confusion, I do not work Microsoft. I just ask them questions.  :)

Tony.