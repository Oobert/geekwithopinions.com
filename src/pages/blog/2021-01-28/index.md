---
path: "/2021/01/28/the-milwaukee-slack-survey-results/"
date: "2021-01-28T02:26:00Z"
title: "The Milwaukee Slack Survey Results"
categories: [""]
tags: [""]
excerpt: ""
---


Back in November, I asked the Milwaukee Slack Community to answer a few questions about their usage and demographics of the slack. The goal was to get a better idea of what the community looked like and what the impact could be if more effort was put into the community. Thank you to all of you who took the time to fill our the survey! What follows are the results of that survey.

Before we get to that, I want to acknowledge that the survey was closed on December 1st, 2020 and as I write this it is January 26th, 2021. I could put a long list of excuses and explanations here but it would covering up the main reason. I have some anxiety over the work required. We will explore this in another post. Until then, let's look at the results.



# The Results

Two things to note. Response count isn't being shown to keep data as confidential as possible. I will say that the number of response was much higher then expected. The other thing to note is why the graphs change towards the end. This is because I started to look at the data in different ways. Meaning I generated graphs from the raw data versus Google Form creating graphs of the basic data. 


## General
### How many hours a week are you active?
![Time Spend on the slack](hours.png)
### Where are you most active?
![Where most active](most-acitve.png)


## Demographic
### What is your age?
![Age](age.png)
### What is your gender?
![Gender](gender.png)
### Do you identify as a person of color?
![People of color](person-of-color.png)
### Do you identify as LGBT+?
![lgbt+](lgbtq.png)


## Comfort to post in public channels
### Do you feel comfortable posting in public channels - ALL?
![Do you feel comfortable posting in public channels - ALL?](comfortable-public-channels.png)

### Do you feel comfortable posting in public channels - Male?
![Do you feel comfortable posting in public channels - male](male-comfort-level-posting.png)

### Do you feel comfortable posting in public channels - Female?
![Do you feel comfortable posting in public channels - Female](female-comfort-level-posting.png)

### Do you feel comfortable posting in public channels - Person of Color?
![Do you feel comfortable posting in public channels - Person of Color](PoC-comfort-level-posting.png)

### Do you feel comfortable posting in public channels - LGBT+?
![Do you feel comfortable posting in public channels - LGBT+](lgbtq-comfort-level-posting.png)

## Effects of a better code of conduct
### Would a better CoC and administration increase comfort to post in public channels - All?
![Better CoC increase comfort to post in public channels - ALL](better-coc.png)

### Would a better CoC and administration increase comfort to post in public channels - Male?
![Better CoC increase comfort to post in public channels - Male](Male-better-CoC.png)

### Would a better CoC and administration increase comfort to post in public channels - Female?
![Better CoC increase comfort to post in public channels - Female](femail-better-CoC.png)

### Would a better CoC and administration increase comfort to post in public channels - Person of Color?
![Better CoC increase comfort to post in public channels - Person of Color](PoC-better-CoC.png)

### Would a better CoC and administration increase comfort to post in public channels - LGBT+?
![Better CoC increase comfort to post in public channels - LGBT+](lgbtq-better-CoC.png)


# My thoughts

The first draft of this section had some words about the Milwaukee Slack being about 10-15% that weren't straight white male. Clearly having ~10% of people identify as a Person of Color, ~10% as LBGT+ and ~14% female equaled 10-15%, right? This is what I expected to see so my that is what I saw. But as I continued to write, I realized that isn't how math works. Back to the data I go! After filtering the results to show only Male, White, and don't identify as LGBT+ responses we have the real number. To my surprise, ~65% of respondents fell into that group. 

As a community that has been self managed, this is a good start. As with life, there is opportunity to do and be better. It would be awesome to see a close to even of a split between all the demographics.  

The next item that was interesting is that the comfort level to post in public channels was similar between all the demographics (sorry about the random graph color changes). There are similar results for data of increased comfort if a better Code of Conduct and administration was in place. 

Lastly, let's chat about the free form comments. The comments ranged from complaints to suggestions to observations. As one could guess, there was a lot of opposed comments on what needs to happen. For example, some suggested forcing the use of real names while others prefer to have the ability to stay anonymous. One of the common things that came up why Slack and can we pay for Slack. This is easy enough to answer now. So, let's do that.

# Why Slack?
This community started on IRC. IRC is great in a lot of ways but isn't use friendly. Slack came a long and made it easier for more people to become involved. Having been around since the IRC days, Slack usage took off pretty much right away. Today, the Milwaukee Slack stands at over 2000 members. However, Slack, as a platform, is far from perfect. The two main issue are the message limit on the free tier and the quality of the admin tools (which are almost non-existent). 

Why don't we move to <insert platform here>? We could. However, Slack has one thing going for it that the other platforms don't. It is used and open for access at more companies in the Milwaukee area. Much of the Milwaukee Slack's usage is during typical work day hours by people on their work computers. Many employers block majority of other platforms. 

Why not pay for Slack? Right now, the Milwaukee Slack averages about 350 active users a week. Slack's pricing model is based on active users per month. The lowest tier costs $6.67 per active user per month. 350 active users x $6.67 equals ~$2,300 a month or just under $30,000 a year. There is the assumption that usage would increase dramatically if the Milwaukee Slack moved off the free tier meaning the cost would increase. I have had conversation in the past with a few companies that expressed interest in sponsoring the Slack and talks quickly fizzled once the price was calculated. 

And that is why Slack continues to be the path. For better or worse. 

# What is next?
That is where we are going to stop for this one. The only thing I know that is going to happen next is to write another blog post about my anxiety and concerns. As for the Slack and the community, there are a lot of ideas floating around. However, I think it will be helpful to express my concerns and talk more about the work before anything gets started.    