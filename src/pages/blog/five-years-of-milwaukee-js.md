---
path: "/2019/03/26/five-years-of-milwaukeejs"
date: "2019-03-26T16:50:27Z"
title: "Five Years of MilwaukeeJS"
categories: [""]
tags: [""]
excerpt: "In December 2018, I stepped down as organizer of the Milwaukee JavaScript meetup (MKEJS) after 5 years. What follows is telling of my experience over the last 5 years. "
---

In December 2018, I stepped down as organizer of the Milwaukee JavaScript meetup (MKEJS) after 5 years. What follows is telling of my experience over the last 5 years.

In early 2014, I was very much a [shy person](https://www.geekwithopinions.com/2014/10/16/my-bout-with-shyness/). At the time, the Milwaukee Slack didn't exist but an IRC channel did. There was a handful of us hanging out in there during the day. Among the users was Dustin Filippini who was the lead organizer of MKEJS at that time. 

Well on some day in early 2014, Dustin was looking for ideas for presentation for MKEJS. At that time, [NodeSchool](https://nodeschool.io/) events were becoming a thing so I tossed the idea of doing a NodeSchool workshop. In no time flat it turned into March 2014's topic and I was organizing it. I was more than a little freaked out. I didn't know what I was doing, have barely used node at this point and was about to run a workshop. If you have read my earlier post about my bout with shyness, you know already, that I was looking to become more active in the Milwaukee dev community. So I went with it and, knowing me, probably whined a little bit about it. To date, this has been one of the better choices I have ever made.

Fast forward a few weeks, the RSVPs were getting up there. [51 RSVPs to be exact](https://www.meetup.com/milwaukeejs/events/168041402/). I have never taught a workshop. Heck I have never presented before. But onward we went. Dustin and I did reach out to other JavaScript devs for help as "tutors" but in the end I was the lead. For a first time out, the event went pretty well. Everyone learned some node, I learned a bit about presenting, and the pizza was free. I even did a bit of living coding on the early exercises. 

Even today, I don't know why but shortly after the March meetup, Dustin made me a co-organizer. And this is how I accidentally became an organizer of a meetup. It wasn't by choice per say. Granted I could have said no but I was looking to become more active in the community. So onward we went. I don't remember who got added when by there were 4 organizers: Myself, Dustin, Paul Sanchez, and Ryan Anklam. 

At this time, the meetup was pretty easy to run and stayed that way for the following two years. It was easy because Corvisa (which became Shoretel which became Mitel) provided the space and ordered the pizza. All we had to do was find speakers and show up.

While running the meetup was easy, things started to change almost from the start and never really stopped. In 2013, Ryan was helping from California as he had moved there for a job at Netflix. In spring of 2014, Paul also left Wisconsin to Chicago land. Then in August 2014, Dustin moved out of state and transferred lead organizer to me. Dustin, Paul, and Ryan were still doing what they could remotely but I needed help. At this point, we asked Stacy London to join us as a co-organizer. She accepted and did a great job helping bring more diverse speakers and attendance to MKEJS. Then in Late 2016, Stacy also left the Wisconsin.

To be clear, there are no hard feeling from me for anyone leaving Wisconsin. I am grateful to have been able to work with all of them. This did lead to joke of when was I going to leave the Milwaukee area. Let me explain. During 2014 to 2016, a lot of Milwaukee based meetup organizers left Wisconsin for a variety of reasons. Wisconsin lost some great talent, who have went on to do amazing things like work at Netflix, Atlassian, Apple, Mozilla, and be on the board for TC-39. Anyone who says Milwaukee doesn't grow great talent doesn't know what they are talking about. But that is another post for another time.

I digress. In 2016, things were still pretty easy as far as running the meetup is concerned. However more changes where coming. Corvisa went through a few changes and was now Shoretel which was no big deal. That was until late 2016, Shoretel pulled the plug on paying for pizza which by this time was about $200-300 for each event but allowed us to continue using their space. Fund raising time!

Work suddenly needed to be done to figure out how to buy food, well pizza. Do we even need food? Well after some trial and error, I quickly learned that a business will happily write a check to another business but not to individual. After talking it over with my awesome wife, Erin, we decided to go into business together. March 2016 is when DevWi, LLC was born. The LLC was created for 2 reasons. First, to make it easier to get sponsorship and to have some separation from the meetup and our personal assets.

A major thing I learned at this time was about liability. Standard I am not a lawyer preface here. As an organizer, you are running an event. You are likely somewhat liable for anything that happens during that event. It gets a bit weird when you factor in being hosted in a business and if there is alcohol served at the event. After learning this, we quickly picked up a small amount of business insurance because we like living in our house.

Back to the story. Thankfully I worked at Northwestern Mutual by this time and had learned that they wanted in on the meetup sponsorship game. Which was great because I had just the deal for them. In 2017, they became the first paid sponsor of MKEJS.

At the end of 2017, Shoretel downsized their office space which meant they would no longer have access to the great meetup space we came to know and love. MKEJS needed to find a new home. This is when I found out that no one in Milwaukee really has space for a good sized meetup. See in 2014, MKEJS had ~200 members and 20-50 RSVPs per meetup. At this time in 2018, MKEJS had over 1000 members and 50-100 RSVPs per event. There are not many spaces in Milwaukee that can hold upwards of a 100 people. Finding space was a hard problem to crack. Either a company would have space but not have it available consistently or would want to have it available consistently. Many places only wanted to host once or twice. Other places wanted to host but couldn't handle the crowd that MKEJS brought in. It was a struggle because one of the things that I strived for was consistency. Keeping MKEJS at the same place at the same time meant it was more accessable to everyone. Basically it lowered the barrier to entry for new people to come. 

Enter [Rokkincat](https://www.rokkincat.com/). They open their doors to MKEJS and hosted us for all of 2018. It worked out great and we are very thankful for this.

In the second half of 2018, I was on cruse control and doing just enough to keep the meetup running. The reason for this is I was at a tipping point. I had a need to do something different and/or something more. It boiled down to I needed a change. So the choice was to put in more effort to grow the meetup in new and interesting ways or to pass it on to the next organizer. Ultimately, I decided to pass the meetup on. This doesn't mean that I am done in the community space. Far from it. Just my time is now focused on other opportunities.

And that is my story of the last 5 years as a meetup organizer. Before we move on, I want to take a moment to say thank you to all our sponsors, speakers, organizers, and attendees over the years. You are all amazing and it was with your help that MKEJS is now nearing 2,000 members. It was great to work and talk with all of you over the years. Keep doing great things!!

## Takeaways
Here are some of my takeaways from the last 5 years. These maybe interesting or helpful to those that are running a meetup or thinking of running a meetup. Honestly, I am just putting these here as a reminder to myself if and when I start another event.

### Rich and Famous
If your motivation is to get Rich and Famous, this isn't the path for that. Ask your self, how many meetup or conference organizers do you know? With that, more people know my name today than 5 years ago. However, there is still much of the community that doesn't know that I or MKEJS exists. Heck, much of the community still doesn't know Milwaukee has [90+ meetups](https://devwi.com/groups). 

With that said, your personal network will grow. This may net you a job offer or net you a higher status in the community which in turn may get you more money or status. Also, the LLC did turn a profit both years it ran the meetup, it wasn't enough to compensate for the work put into it. I wasn't doing this work for money, so this was 100% OK to me.

### Sponsorship
If you are running the meetup as individual or small group, finding sponsorship can be hard. It really depends on your personal network. Over the years, we tried a few different ideas to see if we could get new and different sponsors in the door. We had limited success. This is likely due to 2 things. First, I am likely not great at sales. And second, there isn't currently an appetite for Milwaukee based companies to sponsor meetups. At least there wasn't at the time I ran MKEJS. If you look around, you will see the same handful of business doing most of the sponsoring.

This turned finding sponsor into a another job. To do that job well, you need to know why a business should sponsor, what value they will get, etc. Think of it like pitching to VCs but on a smaller (much smaller) scale.

The other trend that impacts sponsorship is many meetups are now run by a local business. MKEJS is now run by MacGregor Partners and Nvisa for example. Running a meetup as someones day job makes everything just a little bit easier because then the work can be done as part of someone's day job vs having to find time to do the work in your personal time.

### Food
Food... How I loath thee... The only cost effective food option is pizza. I have eaten my fair share of pizza. Pizza is silly low cost per person compared to anything else. I had dreams of having better options for food. Every time I looked into it, I died a little inside. Pretty much every other option was at least double in cost compared to pizza. For a meetup of say 50 people, pizza would be about $300 and anything else would be at least $600. I apologies for all the pizza I served you all.

### Speakers
Finding local speakers can be rough. For JavaScript there is a small group of people that are willing and able to give presentations. The good news is the pool is slowly growing. One of my hopes of passing on MKEJS is that the new organizers will tap their networks and find new people. There is the start of a new trend of businesses allowing and pushing their employees to contribute to the community by giving presentations that the employees worked on at work. This is very encouraging as many of the speakers at MKEJS over the years created their presentations on their own time.

To the businesses, managers, and leads reading this. If you want to make recruiting a little bit easier, then get your current employees to talk about the cool work that is happening on your teams. If they give presentations where they are excited to talk about their work, this will get others interested in the work that your are doing. I believe this is among the best ways to do recruiting and get your work known in our community. Let your people show off the work they are doing. Oh and if you follow this advice, your employees should be allowed to work on their talks at work as it is benefiting you as much as it is them.

To the speakers reading this. Giving a talk is all about telling a story. Preferable true story. After seeing dozens of talks, I have strong opinions on what makes a good talk. First, if your talk is 40mins of content that I can learn from Google in 10mins, please rethink your talk. To put it bluntly, this is not great use of everyone's time. Your talk should excite or inspire the audience. Make them want to go out and learn more. The best talks I have seen spend about 15mins doing a quick getting started overview then spend the rest of the time blowing the audience's mind with what is possible. I remember a React talk by Sam Reed where I didn't understand how or what he was showing us but knew that I needed to learn react. The next best talk is someone that walks you through their experience while using a tool, framework, pattern, etc. Going deep into what they learned and giving context for their learnings. The reasons both of these work is they provide the audience with information that isn't easily searchable.

### Diversity
Diversity in dev in Milwaukee is an interesting subject. It was one of the areas of the meetup that I always wanted to do better. The bare minimum to show that your are invested in diversity is to have a code of conduct and enforce it. The next step after that is to actively search for a diverse set of speakers and invite them to speak. A diverse attendance will follow. During the last few years, I was informally tracking who came to the meetup. MKEJS averaged in the ballpark of 10-20% women and slightly higher for non-white guy.

This is probably the hardest things to do well as a meetup organizer. There is some hope though. I am working on a post about the demographics in Milwaukee and early numbers suggest that the split between men and women is closer to 50/50 than many would have guessed. Keep trying!

### Motivation
Early on, my motivation to be active in the dev community was because I wanted to have conversations about tech. To this day, I love talking about the awesome work people are doing. I love to talk about the awesome work that I was doing. Basically, this was a great way for me to learn and grow as a person. On top of that, I wanted to get over my shyness. As time went by, my motivation changed. See when I started helping MKEJS, "we do fronted development" meant we do jquery. But over the years, Milwaukee evolved and now have companies pushing the cutting edge of frontend development. During this transition, my motivation changed to wanting to do my part to help facilitate this change. To do what I could to make Milwaukee that much better.

Thank you for reading!