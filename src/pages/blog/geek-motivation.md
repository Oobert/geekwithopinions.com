---
path: "/2013/10/16/geek-motivation/"
date: "2013-10-17T01:54:38Z"
title: "Geek Motivation"
categories: ["General"]
tags: ["Career", "life", "Programming"]
excerpt: "Physcology is something I have always been interested. If I couldn't work in software development, ..."
---

Physcology is something I have always been interested. If I couldn't work in software development, I would probably be in physcology. To more specific, I am interested in geek physcology. What makes us act they way we act? What motivates us to be engaged? How are we the most productive?

Recently there seems to be shift in how people are being managed at work. More specifically how people in creative positions are being managed. Traditional styles of management seem to be less productive than newer styles of management. The reason seems to be that the newer styles of management help make a geek’s work life better by giving them freedom to complete their tasks in a way that works for them.

Michael Lopp, who blogs under the name Rands, talks a lot about soft skills. He has a posts entitled [“The Nerd handbook”](http://www.randsinrepose.com/archives/2007/11/11/the_nerd_handbook.html) and [“Managing Nerds”](http://www.randsinrepose.com/archives/2011/01/17/managing_nerds.html). These posts outline many of the characteristics that generally define geeks. One of the main themes throughout each post is something dubbed the high. The high is the euphoria that is felt when one understands or complete a task. Much likes drugs, this euphoria is what geeks are chasing. Todays new management trends are trying to create environments where geeks can reach this high quicker because this is when geeks create awesome.

This high is important. Without the high, geeks get frustrated, bored, and quit. Ever wonder why some geeks seem to switch jobs every few years? It is because they have an understanding of all the interesting problems and have dominated those problems. There is nothing else for them to do to reach the next high. So they move on.

Having solved all the interesting problems is not the only reason geek quit. Sometimes it is because of the environment they work in. Did you know a business can have a mindset? Humans, groups, teams all have a distinct mindset that drives the actions and culture of that collective. There are two main types of mindsets: fixed and agile. Geeks do not liking being in a group that is a fixed mindset.

A person with an agile mindset craves knowledge and is ok with failure as long they are learning. These are the people that try 10 different algorithms to sort a list to find out which one is best. These are the people that suggest cutting edge technologies because they want to learn it. They know it will be painful to implement but they don’t care. These are the people that want to be the least skilled person in a room because they know the other people in the room have knowledge they can learn.

A fixed mindset person is one that believes they are naturally smart. The people in this mindset have been typically told they are really smart. These people tend rely on their natural ability than trying to get better and learn. They are easily frustrated with failure. If possible they would prefer to be the smartest person in the room as it somehow validates what they believe is true.

Linda Rising gave a [talk](http://www.agilealliance.org/resources/learning-center/keynote-the-power-of-an-agile-mindset/)on subject a few years back. She explores this topic in more detail. During her talk, she suggests that a businesses can also have a mindset. I believe this to be true. Businesses show the same charistics as people do but with the side effect of this will affect their employees. A fixed mindset business will tend to not tolerate failure. They will assume the talent of all the employes will carry that business forward. An agile mindset business will allow employees to fail as long as they move forward.

Geeks prefer to not work for fixed mindset businesses. Geeks love to try new things and push themselves to be better and learn. In a fixed mindset business where failure is not an option, trying new things is also not an option. Which leads to geeks getting frustrated and leaving.

On the flipside, geeks prefer working for a business with an agile mindset. Companies like Github and Netflix are embracing this mindset and attracting highly skilled geeks. Github believes in giving their geeks almost unlimited freedom for when and how they work. For them this has worked very well. Their employees are highly motivated and engaged.

If you are a geek and want to make awesome stuff and to have an impact on the world in some small way, I would highly suggest seeking out a company that understands how geeks work and function the best. Your utopia exists but it is up to you to find it.

If you are a business that is looking for motivated geeks, I suggest that you make sure to take care of your geeks. Give them the space and opportunity to fail and learn. Given the right environment, your geeks will create awesome.