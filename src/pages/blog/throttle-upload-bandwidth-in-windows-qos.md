---
path: "/2013/05/24/throttle-upload-bandwidth-in-windows-qos/"
date: "2013-05-25T02:26:08Z"
title: "Throttle upload bandwidth in Windows (QoS)"
categories: ["General"]
tags: ["IT"]
excerpt: "I have digital cable service from Time Warner. It is not the great but still decent. Currently my u..."
---

I have digital cable service from Time Warner. It is not the great but still decent. Currently my upload speeds are limited by Time Warner at 1Mbps. The highest I can get in my area is 5Mbps but it is costly. My point is I have an upstream problem. If I want to share files of  any sort of size, it takes forever and slows everything else down as you need both up and down to do anything on the internet.

Recently I have found a solution and it has been in Windows since Vista. Also in Win7, Win8, Server 2008 and 2008R2\. Windows ships with Bandwidth throttling specifically for uploads. The following is how to set it up.

Start off by opening up the group policy editor. To do this open the start menu, type "gpedit.msc" , and hit enter. The edit should open, just navigate to "Policy-based QoS" which is found in Computer Configurations -> Windows Settings. This can also be for the user configuration too.

[![gpedit](25-1.png)](http://www.geekwithopinions.com/wp-content/uploads/2013/05/gpedit.png)

Right Click on "Policy-based QoS" and select "Create New Policy..." This will start the wizard.

In the following windows you can setup how and what the policy will throttle. In step 1, the policy is named and this is where the maximum outbound (upload) speed is set.

[![step1](25-2.png)](http://www.geekwithopinions.com/wp-content/uploads/2013/05/step1.png)

In step 2, an specific application or url can be setup to be throttled.

[![Step2](25-3.png)](http://www.geekwithopinions.com/wp-content/uploads/2013/05/Step2.png)

In step 3 the source or destination IP can be set.

[![Step3](25-4.png)](http://www.geekwithopinions.com/wp-content/uploads/2013/05/Step3.png)

And lastly in step 4, the source and/or destination port can be setup to be throttled.

[![Step4](25-5.png)](http://www.geekwithopinions.com/wp-content/uploads/2013/05/Step4.png)

For me, I can typically set limits on an application or just the computer as a whole as I have such little bandwidth to go around. This has become really useful for applications like Google Drive Sync that does not have native support for throttling bandwidth.

A side note, more and more consumer routers are able to do QoS and bandwidth management but that is a different post altogether.

EDIT: In some cases, a registry key may need to be changed in order for this to work. More info can be found at the following link. [http://support.microsoft.com/kb/2733528](http://support.microsoft.com/kb/2733528)