---
path: "/2014/01/30/one-mans-solution-for-full-page-web-apps/"
date: "2014-01-31T03:54:56Z"
title: "One man's solution for full page web apps"
categories: ["General"]
tags: ["Design", "Programming"]
excerpt: "Recently, I had a need to create a web application that functioned kind of like a native app. The d..."
---

Recently, I had a need to create a web application that functioned kind of like a native app. The design requirements were simple. There would be a header and menu stuck to the top of the page and a footer and navigation buttons stuck to the bottom of the page. Content would fill the middle.

There are many ways to do this. For example,[bootstrap has an example](http://getbootstrap.com/2.3.2/examples/sticky-footer-navbar.html "sticky footer example") of how to do this type of layout. This is probably the most common example of how to keep elements stuck to the bottom of the page. It has one problem, the height of the stuck element must be known. Sadly the height for me is not always the same which makes this example not ideal.

Flexbox is another option. It would work good except for its browser support. For this project I had the fun requirement of needing to support IE8\. Don't ask, long story, some day I will tell it.

So how did I solve my problem?  [CSS Table Display](http://caniuse.com/#feat=css-table "can i use")! CSS Tables allow you to use semantically correct layout with divs (or newer tags) but have those divs act as if they were table elements. So one can read all about that what they are and how to use them all over the Googles. The remainder of this post is what I did with them.

Below is some example html and css I used. Header, menu, content, footer, and bottom stuff are all normal display block div tags. They work like you would expect. It even works will to drop in Bootstrap grid system to place things on different places horizontally. 

The key to the is the div with class .trow-fill. It will push all elements below it to the bottom of the page because its display is set to table-row. Make sure to notice that the body tag is set to display as a table. What is cool about this is the content or the navigation area can change heights and everything will move correctly. The stuff that should be at the bottom of the page will stay there.

Screen shot:
[![layout](31-1.png)](http://www.geekwithopinions.com/wp-content/uploads/2014/01/layout.png)

Example HTML and CSS

```html

<html>
<head>

<style>

	body{
		height: 100%;
		width: 100%;
		margin: 0;
		display: table;
	}

	.trow-fill{
		display: table-row;
		height: 100%;
		background-color: yellow;
	}

	/*fill colors for effect*/
	.header{
		background-color: green;
	}

	.menu{
		background-color: orange;
	}

	.content{
		background-color: red;
		height: 150px;
	}

	.bottom-stuff{
		background-color: lightblue;
	}

	.footer{
		background-color: grey;
	}
</style>

</head>
<body>

	<div class="header">Header</div>
	<div class="menu">Menu</div>

	<div class="content">content</div>

	<div class="trow-fill" >Normally this would be empty. This will push anything elements below it to the bottom of the page.</div>
	<div class="bottom-stuff">Navigation and other stuff. This can be dynamic height!!</div>
	<div class="footer">footer</div>

</body>
</html>
```

Now a few things may be running through your head. Mainly something about tables and it feeling dirty. Well I was with you up until I noticed something about Bootstrap. In v2.X of bootstrap, CSS tables is exactly how it achieves the fluid grid system. [Just look right here.](https://github.com/twbs/bootstrap/blob/v2.3.2/docs/assets/css/bootstrap.css#L397 "bootstrap") If it is good enough for them, it is good enough for me.