---
path: "/2014/09/19/stop-chasing-carrots/"
date: "2014-09-19T13:59:30Z"
title: "Stop chasing carrots"
categories: ["General"]
tags: ["Career", "Soft Skills"]
excerpt: "My first job out of college was working for a large printer of magazines.  There was a good number ..."
---

My first job out of college was working for a large printer of magazines.  There was a good number of developers and for the first few years things were good. The company had some interesting abilities outside of print that I thought were really interesting and I stayed longer than I should have because the potential to get bigger was there.

Then 2008 happened. We all felt the crunch. Print is not really a growing industry and the recession did not help. Yet, I stayed. I stayed even after new projects required over time from day one. I stayed even though perks and benefits of working for this company disappeared because they needed to make the bottom line look better. I stayed for another 3 years. Why? Because I was chasing the what could be. The metaphoric carrot  if you will. I believed the promises. I was a fool.

During those years, I was consistently underpaid when compared to market average. The work environment went from happy to hostal. Time lines went from reasonable to this needs to be done yesterday.

One day the carrot was removed. The CEO stood in front of the company and said they were going all in on print which mean the interesting abilities outside of print were being sidelined for projects that pushed print. I was done. I stopped working overtime. My work suffered. It didn't take long before I left.  I couldn't chase a carrot I didn't believe in.

This experience taught me a major lesson:

**Stop chasing carrots**

This is not an original idea. This is an idea that is supported by science and research. When a task requires even the slightest cognitive processing, carrots no longer work. In fact they tend to be more harmful than helpful. Dan Pink's TED talk [The Puzzle of Motivation](http://www.ted.com/talks/dan_pink_on_motivation "Dan Pink on motivation") sums this up very well.

In Dan's talk he suggest that people who work in creative fields require different motivators. Motivators that you have right now, not that you may get at some point in the feature. Dan suggests 3 different items:

* <span style="color: #292f33;">Autonomy - Can you do the work in a manner that works for you?</span>
* <span style="color: #292f33;">Mastery - Do you have a chance to learn and grow?</span>
* <span style="color: #292f33;">Purpose - Does the work affect the world in even the slightest bit?</span>

There are many other factors but these 3 are important. Looking back on my relatively short career, I have to agree. My biggest issues were doing things I did not believe in and doing them in a way that didn't work for me. Going back to my story above, I left that job because I no longer had a purpose I believed it. It made me sad that the focus of the company switched because those areas outside of print had purpose and were awesome.

Whatever you are doing are doing for a job, make sure you are getting something from it. If you are working 80 hours a week because you may get a raise, you are doing it wrong. It is very likely that you will not get anything and soon this type of effort will become expected from you. If you are working 80 hours a week because you love what you are doing and you are making that choice then I am not going to tell you not too.