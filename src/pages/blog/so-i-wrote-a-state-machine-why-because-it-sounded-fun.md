---
path: "/2014/02/07/so-i-wrote-a-state-machine-why-because-it-sounded-fun/"
date: "2014-02-08T01:26:54Z"
title: "So I wrote a state machine. Why? Because it sounded fun!"
categories: ["Code Things"]
tags: ["JavaScript", "Programming", "State Machines"]
excerpt: "I had a problem. I was tasked to make a wizard type interface for a few workflows in an web app. Th..."
---

I had a problem. I was tasked to make a wizard type interface for a few workflows in an web app. The workflows had 3+ steps with the current max number of steps at 10.

**Options**

Option 1: I could control state on the server. After every step in the workflow the data of that step would be posted to the server where the server would keep track of it in session and take the user to the next step. This is very doable, and is pretty much how web apps have functioned before ajax. The downside to this is controlling partial state on the server is hard because session management is hard. You have to account weird scenarios like what happens if the user starts the same workflow in a different browser window, you now have to somehow identify what window goes to what session. Or how do you know when to clear a workflow in progress because the user navigated to a different page in the web app. What happens if they come back? All of these question can be fixed in some form but normally involve a lot of if statements.

Option 2: Wouldn't it be easier to keep all workflow data on the client until the workflow was completed? Yes, yes it is. However this means that the client can no longer do full page reloads between steps. No problem, there are frameworks that cover this such as [Angular.js](http://angularjs.org/ "angularjs"). In my solution, I am loading and unloading html templates into the DOM manually and using [Knockout.js](http://knockoutjs.com/ "knockoutjs") for data binding. Why did I roll my own this way? Because IE8 but that is a different blog post. By keeping all the workflow state in the browser, we have less issues to deal with but a few new ones come up. For example, do you care that the user has to start over if they hit refresh? Do you need the browsers back button to work? These were easy for my use cases, it didn't matter at the moment because of how this will be used in production. I started down this road, things were going well. But then I noticed that my JavaScript was getting kind of cluttered with if statements such as...

```javascript

if (here) do this
else if (over there) do that
else if (holy crap) I have no idea
else if (another one?) and I am lost
```

Option 2b: State machines! About 2 steps into the first workflow, I noticed a pattern. Every step in a workflow loaded something, waited for the user to do work, moved to the next step. The lightbulb went off and I started looking at state machines in JavaScript. I found many like [machina.js](http://machina-js.org/ "machina.js") and npm had many in there as well. [machina.js](http://machina-js.org/ "machina.js") being the first in my search results, I went with it. It looks good and probably would have solved my problem but it has(had?) a dependency on [underscore.js](http://underscorejs.org/ "underscore.js") Due to the nature of this project, introducing an external library is time consuming, introducing two is a huge pain. But, you guessed it, that is another post someday. In the end, I decided to build my own. Why? Because it sounded fun, also I didn't need a full featured library, yet.

**Code!**

So I wrote a state machine. It had a few requirements there were identified upfront.

* Know what was the current state
* Be able to change to a new state
* Call an unload method on the old state
* Call a load method on the new state
* Pass data to the new state
* Be able to generically call methods on the current state

Over time, I am sure the requirements will grow and we will make the choice of growing this code base or moving to a more feature complete option. And here it is.

[javascript]
    var fsm = function (states) {
        this.current;
        this.states = states;
    };

    fsm.prototype.changeStateTo = function (newState, obj) {
        if (this.current &&
            this.current.unload) {
            this.current.unload();
        }

        if (this.states[newState]) {
            this.current = this.states[newState];

            if (this.current.load) {
                this.current.load(obj);
            }
        }
    }

    fsm.prototype.callAction = function (action, obj) {
        if (this.current[action])
            this.current[action](obj);
    }
[/javascript]

As you can see, the state machine takes in an object that is the different states that it can be. A usage example is below. 

The changeStateTo function will call unload on the current state, and then call load on the new state. It has some light error checking to make sure states and methods exist before continuing.

The callAction method is a generic way to call a specific action (function) on the current state. An example of this would be if there is a button that is on every screen, you could use this method to call that action when it is pressed on the current state.

And a small example of usage.

```javascript

    var myFsm = new fsm({
        state1:{
            StateRelatedObject: { 
              text: "hello"  
            },
            load: function ()
            {
                //do work like load template or show/hide page elements
            },
            StateRelatedFunction: function()
            {
                //do specific work related to this state.
                //can access objects or methods on current state like...
                this.StateRelatedObject.text = "hello world";
            },
            unload: function()
            {
                //clean up after yourself here.
            }
        },
        state2:{
            load: function () { },
            StateRelatedFunctionOrObjects: function() { },
            unload: function(){ }
        }
    })

    myFsm.changeStateTo("state1");

    myFsm.callAction("StateRelatedFunction", { /* data in here */ });

```

The object that is passed into the state machine can get rather large. This is ok because it is segmented into it different states and is well organized.

Testing is pretty easy too!

```javascript

    //setup test parms here.

    myFsm.state1.StateRelatedFunction();

    //do asserts on data here.
    //example: myFsm.state1.StateRealtedObject.text === "hello world";

```

Enjoy!

Edit 03/06/2014: I fixed a misspelling in code. I also posted a complete code example to github.
[https://github.com/Oobert/LittleStateMachine](https://github.com/Oobert/LittleStateMachine "Little State Machine")