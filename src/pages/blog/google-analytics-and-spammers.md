---
path: "/2009/05/18/google-analytics-and-spammers/"
date: "2009-05-18T22:36:29Z"
title: "Google Analytics and Spammers"
categories: ["General"]
tags: []
excerpt: "I decided to try out Google Analytics over the weekend. My web host's web stats leave something to ..."
---

I decided to try out [Google Analytics](http://www.google.com/analytics/ "Google Analytics") over the weekend. My web host's web stats leave something to be desired. Since Google already knows everything about me and my site already, I figure it was a safe things to do. With a [WordPress blog](http://wordpress.org/ "Word Press"), it is very easy to set up. Here are the steps.

1. Sign up for[Google Analytics](http://www.google.com/analytics/ "Google Analytics")
2. Added a bit of JavaScript to footer.php
3. ???
4. Profit! :)

It literally took five minutes to set up.  Google Analytics has some very cool features.  For example, it keeps track of return visitors vs new visitors. It is pretty neat.

Google Analytics comes with a free side effect! The stat tracking requires the client to have JavaScript execution enabled. What this means is that clients without JavaScript enabled do not get tracked. So if you have a lot of users that disabled JavaScript, this could be back. However there is a an upside, Spam bots do not run JavaScript! I know this because Google Analytics only tracked me for the first day that I had it installed. However during that same time frame I had ~20 spam comments caught. I am happy about this because I get a more accurate view of the visitors to the blog.

Tony.