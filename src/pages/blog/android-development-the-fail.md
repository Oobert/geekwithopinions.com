---
path: "/2011/02/07/android-development-the-fail/"
date: "2011-02-07T22:00:26Z"
title: "Android Development: The Fail"
categories: ["General", "Project Updates"]
tags: ["Android", "Programming"]
excerpt: "So back in October I started the process of learning to do development for to android. Sadly, I am ..."
---

So back in October I started the process of learning to do development for to android. Sadly, I am here today to admit that went poorly. This is my reflection on why it went badly and how I expect to better next time.

Problem 1: Me. Yup, that is right. The first problem I ran into was me. I have a lot of ideas for applications for Android and in general. The problem isn't that I jumped right into development, it was when I jumped I landed in a spot that was over my head and that made me instantly frustrated. With in days of installing the tools and SDK my drive had died. I just got over whelmed with all the new that I didn't take the time to learn.

Solution: While I hate doing the typical "Hello World" type apps, it is a necessary evil. It is the steps needed to ramp up understanding of the environmental. What I was trying to do was not hard but because I didn't know the basics of Android development, it was difficult to find help. The process isn't hard and takes a bit of time but ramping up is needed to keep from being overwhelmed.

Problem 2: Holy cow, embedded application development is slow. Let me be clear, development isn't slow but getting you application to run via emulator or on a device is painful. When it takes a few minute for my application to run, this just kills the fun and drive to continue. Especially when learning. When I was waiting, it was so easy to get side tracked.

Solution: I hear the sdk and emulators are getting better and faster. I am also in needed of a new PC which is pushing 5 years old or at least an upgrade or 2\. Other than that, I am not sure what else I can do.

So the biggest problem was me. Once I can get through them, it should be pretty easy to continue. I plan on starting this again as I think there is some positive applications I could contribute to the community. However this is on hold for the moment. Need to get caught up on technologies related to my job, mainly ASP.NET MVC