---
path: "/2019/08/21/the-many-faces-of-the-milwaukee-dev-community/"
date: "2019-08-21T02:26:00Z"
title: "The Many Faces of the Milwaukee Dev Community"
categories: [""]
tags: [""]
excerpt: ""
---

Have you ever wondered what the Milwaukee developer community looked like? Yeah, me too. What do we look like? How old are we? What makes us, well, us? While I ran Milwaukee JS, it was difficult to find a diverse set of speakers. In the end, I want to know if it was our community or my social networks that work lacking.

For those that don't know, I run a meetup.com scraper at devwi.com. It keeps track of some 90+ meetups and plots their events on a calendar. One day, I realised that it maybe possible to answer some of the easier questions about the dev community by looking at meetup.com as the meetup.com api exposes user data as well. A few short hours of coding, the scrapper service started grabbing all user data that was publicly available. 

## The bad news
Meetup.com has fields for name, email, age, gender, avatar and any social media accounts you link. The downside here is no one makes email, age, or gender public except for me because I was testing the api. That sucks for this but is 100% understandable.

## The good news
At this point, I had nothing except a list of users and the name they game meetup.com. I tried to find a name API that would guess gender based on name but I couldn't find one with a free limit high enough for the number of users I had to submit. What can I say, I am cheap. Then I had a thought, a weird thought. While no one made their age or gender public, many of you uploaded an avatar. Many of those avatars are of your face... Yup you guessed it, I did what you are thinking. I put ever avatar through [Azure's computer vision api](https://azure.microsoft.com/en-us/services/cognitive-services/computer-vision/). Sorry... The API spits out gender and age based on its machine learning algorithms. 

I acknowledge the line that exists here. At least the title now makes sense. 🤪

## Results preface
The following results should not be considered, how do we say, accurate but more of a conversation starter. My hope here is someone is willing to share more accurate data and contribute for the conversation. 

I did spot check a handful of users that I knew and the API results where "accurate" enough for this blog post.

Lastly, I did reach out to a few of my recruiter friends thinking that they may have access to good demographic data and just maybe be willing to share. Turns out that asking people for age/gender/race as a recruiter is not a great idea. I should have guessed that... oh well. 

## The data
Total meetup area members: 19,621

Total members with avatars: 9,265

**Graph:**
![Milwaukee Dev Community graph](/MilwaukeeDevCommunity.JPG)

## Hot Take
If I am honest, I was surprised and excited by the breakdown. I thought the ratio between men and women was much worse but am excited that situation is likely not what I had assumed. To be clear, we can do better but overall not terrible for our growing community.