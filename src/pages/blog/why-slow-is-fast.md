---
path: "/2011/05/14/why-slow-is-fast/"
date: "2011-05-14T20:08:06Z"
title: "Why slow is fast."
categories: ["General"]
tags: ["Programming"]
excerpt: "Recently, I have been thinking about an old fable. I am sure that is was read to you as a toddler o..."
---

Recently, I have been thinking about an old fable. I am sure that is was read to you as a toddler or maybe you read it as a kid. _[The Tortoise and the Hare](http://en.wikipedia.org/wiki/The_Tortoise_and_the_Hare)_ is a fable about a hare who challenges a tortoise to a race. The story ends with the slow and steady tortoise beating the hare in the race. One of the interpretation of this story is haste makes waste. I believe this fable has some serious lessons for any one who works with software development.

In college, Computer Science students learn about software development models like Waterfall or Agile Development. The students learn why structure is needed to effectively develop software. They are also taught the downfalls of the lack of structure. I, like may students, learned all of this in college. Recently, this lesson was brought home and I learned first hand why the tortoise is so damn fast.

The hare from the fable has some very appealing attributes on first look. In terms of software development, The hare starts off strong. He produces code sooner and boy can he code. He can also adjust to changes in design and requirements faster. This sounds great doesn't? I mean who wouldn't want all of this?

The tortoise on the flip side has to gather requirements and write designs. This is all before coding can even begin. The tortoise has one thing, a process. He knows that following his process will result in a solid product what out anything missed. So he just plugs away, slow and steady.

The devil is in the details. Or so they say. A deeper look at the hare and the flaws become visible. The hare relies on sheer speed. The sheer speed can only continue for a finite amount of time. There is no time for a process or any structure. This means requirements are missed, code is sloppy, and technical dept is being added at a crazy rate. At some point, the hare will hit a wall. This wall has many forms. In my case, the wall is an endless loop of adding and removing requirements and bug fixes.

The tortoise on the other hand is consistent. The code is produced in a consistent manner. The Tortoise's output is also consistent. Since he has a process, he can accurately estimate when a feature or fix will be completed. In the end, the tortoise will pass by the hare and will do so with a generally better product.

I always knew having a process was needed. Hell, we wouldn't learn it in college or have people much smarter than me publish books on the subject if it wasn't important. The thing I didn't know is the subtleness of the problems that start to creep up. At first it is minor annoyances like having to to fix brittle tests. Before you know it, the Hare will start to cause full blown production problems.

Watch out for the hare. He causes problems.