---
path: "/2013/06/24/enabling-twitter-cards/"
date: "2013-06-25T02:26:41Z"
title: "Enabling Twitter cards"
categories: ["General"]
tags: []
excerpt: "Twitter cards are summary widgets that show up for links to websites that have them enabled. There ..."
---

[Twitter cards](https://dev.twitter.com/docs/cards "twitter cards") are summary widgets that show up for links to websites that have them enabled. There are a few types of cards: summary, summary with image, photo, product, and a few others. To enable twitter cards a few things need to happen. First, the website must include meta info in the page header to tell twitter what to put in the card. Next, the website must request to have cards activated for the site.

This evening I set up twitter cards for this blog.  This blog runs on Wordpress. Luckily there are a handful of plugins that take care of inserting the meta data into the page header. I installed [JM Twitter Cards plugin](http://wordpress.org/plugins/jm-twitter-cards/ "JM Twitter cards"). It had the most downloads and 5 stars. Install was painless, and setup was easy. Just filled out the forms for the summary cards in the settings JM Twitter Cards plugin.

Next I logged into Twitter's dev site where there is a [Twitter Cared Validator](https://dev.twitter.com/docs/cards/validation/validator "twitter card validator") and validated that the plugin was working. Sure enough  everything looked good. So I clicked the submit button for approval. I filled out the form will a few easy questions about my website. Upon submission was told to expect a response within weeks.  It took about 5mins to get a response.

That is it.

Example:

> Enabling [#TwitterCards](https://twitter.com/search?q=%23TwitterCards&src=hash) for your website. [http://t.co/Ob2JUSKl3U](http://t.co/Ob2JUSKl3U)
> 
> — Tony Gemoll (@Oobert) [June 25, 2013](https://twitter.com/Oobert/statuses/349353381875302400)

<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>